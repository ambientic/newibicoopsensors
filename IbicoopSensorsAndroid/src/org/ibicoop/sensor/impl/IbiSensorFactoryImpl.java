package org.ibicoop.sensor.impl;

import org.ibicoop.android.sensor.impl.IbiAndroidSensorAccelerometer;
import org.ibicoop.android.sensor.impl.IbiAndroidSensorBluetooth;
import org.ibicoop.android.sensor.impl.IbiAndroidSensorLocation;
import org.ibicoop.android.sensor.impl.IbiAndroidSensorMetro;
import org.ibicoop.android.sensor.impl.IbiAndroidSensorMicrophone;
import org.ibicoop.android.sensor.misc.SensorBattery;
import org.ibicoop.android.sensor.misc.SensorRadio;
import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorFactory;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorType;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.PackageManager;

public class IbiSensorFactoryImpl extends IbiSensorFactory {

	public IbiSensorFactoryImpl() {
		System.out.println("Constructor of IbiAndroidSensorManager");
	}

	@Override
	public IbiSensor getSensor(Object context, IbiSensorType type, IbiSensorResolution resolution, IbiSensorListener listener) throws IbiSensorException {

		// resolution may be null
		if (context == null || type == null || listener == null) {
			throw new IbiSensorException();
		}

		System.out.println("Get sensor");

		if (type.equals(IbiSensorType.ACCELEROMETER)) {
			if (!((Context) context).getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER)) {
				System.err.println("Can not find accelerometer!");
				throw new IbiSensorException();
			} else {
				System.out.println("Construct a accelerometer!");
				return IbiAndroidSensorAccelerometer.getInstance(((Context) context), resolution, listener);
			}
		} else if (type.equals(IbiSensorType.LOCATION)) {

			if (!((Context) context).getPackageManager().hasSystemFeature(PackageManager.FEATURE_LOCATION)) {
				System.err.println("Can not find location sensor!");
				throw new IbiSensorException();
			} else {
				System.out.println("Construct a location sensor!");
				return IbiAndroidSensorLocation.getInstance(((Context) context), resolution, listener);
			}
		} else if (type.equals(IbiSensorType.BLUETOOTH)) {

			if (BluetoothAdapter.getDefaultAdapter() == null) {
				System.err.println("Can not find Bluetooth sensor!");
				throw new IbiSensorException();
			} else {
				System.out.println("Construct a Bluetooth sensor!");
				return IbiAndroidSensorBluetooth.getInstance(((Context) context), resolution, listener);
			}
		} else if (type.equals(IbiSensorType.MICROPHONE)) {

			if (!((Context) context).getPackageManager().hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
				System.err.println("Can not find microphone sensor!");
				throw new IbiSensorException();
			} else {
				System.out.println("Construct a microphone sensor!");
				return IbiAndroidSensorMicrophone.getInstance(((Context) context), resolution, listener);
			}
		} else if (type.equals(IbiSensorType.METRO)) {

			if (!((Context) context).getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER)
					|| !((Context) context).getPackageManager().hasSystemFeature(PackageManager.FEATURE_MICROPHONE)) {
				System.err.println("Can not find metro sensor!");
				throw new IbiSensorException();
			} else {
				System.out.println("Construct a metro sensor!");
				return IbiAndroidSensorMetro.getInstance(((Context) context), listener);
			}
		} else if (type.equals(IbiSensorType.BATTERY)) {
			System.out.println("Construct a battery sensor!");
			return SensorBattery.getInstance(((Context) context), listener);
		} else if (type.equals(IbiSensorType.RADIO)) {
			System.out.println("Construct a radio sensor!");
			return SensorRadio.getInstance(((Context) context), listener);
		}

		throw new IbiSensorException();
	}
}
