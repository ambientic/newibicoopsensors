package org.ibicoop.android.sensor.misc;

/***
 Copyright (c) 2008-2012 CommonsWare, LLC
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy
 of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
 by applicable law or agreed to in writing, software distributed under the
 License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS
 OF ANY KIND, either express or implied. See the License for the specific
 language governing permissions and limitations under the License.
 From _The Busy Coder's Guide to Advanced Android Development_
 http://commonsware.com/AdvAndroid
 */

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.SystemClock;

public class SensorShaker extends IbiSensor {
	private SensorManager mgr = null;
	private long lastShakeTimestamp = 0;
	private double threshold = 1.0d;
	private long gap = 0;
	private SensorShaker.Callback cb = null;

	//
	//
	//

	public interface Callback {
		void shakingStarted();

		void shakingStopped();
	}

	//
	// Constructor
	//

	static SensorShaker singleton = null;

	static public synchronized SensorShaker getInstance(Context ctxt, double threshold, long gap, SensorShaker.Callback cb) {
		if (singleton == null)
			singleton = new SensorShaker(ctxt, threshold, gap, cb);
		return singleton;
	}

	private SensorShaker(Context ctxt, double threshold, long gap, SensorShaker.Callback cb) {
		this.threshold = threshold * threshold;
		this.threshold = this.threshold * SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH;
		this.gap = gap;
		this.cb = cb;

		mgr = (SensorManager) ctxt.getSystemService(Context.SENSOR_SERVICE);
		mgr.registerListener(listener, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
	}

	//
	//
	// IBISENSOR METHODS
	//
	//

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
	 */
	public void start(IbiSensorListener listener) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#close()
	 */
	@Override
	public void stop() throws IbiSensorException {
		mgr.unregisterListener(listener);
		super.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#pause()
	 */
	@Override
	public void pause() throws IbiSensorException {
		super.pause();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#resume()
	 */
	@Override
	public void resume() throws IbiSensorException {
		super.resume();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
	 */
	@Override
	public void changeResolution(IbiSensorResolution resolution) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#getData()
	 */
	@Override
	public IbiSensorData getData() {
		return null;
	}

	//
	//
	//
	//
	//

	private void restartListenner() {
		mgr.unregisterListener(listener);
		mgr.registerListener(listener, mgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_UI);
	}

	private void isShaking() {
		long now = SystemClock.uptimeMillis();

		if (lastShakeTimestamp == 0) {
			lastShakeTimestamp = now;

			if (cb != null) {
				cb.shakingStarted();
			}
		} else {
			lastShakeTimestamp = now;
		}
	}

	private void isNotShaking() {
		long now = SystemClock.uptimeMillis();

		if (lastShakeTimestamp > 0) {
			if (now - lastShakeTimestamp > gap) {
				lastShakeTimestamp = 0;

				if (cb != null) {
					cb.shakingStopped();
				}
			}
		}
	}

	private SensorEventListener listener = new SensorEventListener() {
		public void onSensorChanged(SensorEvent e) {

			if (e.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				double netForce = e.values[0] * e.values[0];

				netForce += e.values[1] * e.values[1];
				netForce += e.values[2] * e.values[2];

				if (threshold < netForce) {
					isShaking();
				} else {
					isNotShaking();
				}
			}
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			// unused
		}
	};

}