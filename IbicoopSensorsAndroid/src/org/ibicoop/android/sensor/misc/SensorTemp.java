package org.ibicoop.android.sensor.misc;

import java.util.HashMap;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class SensorTemp {
	private final SensorManager mSensorManager;
	private final Sensor mTempSensor;

	private SensorTemp.Callback cb = null;
	private Context context;

	private HashMap<String, SensorTemp.Callback> callbackMap = new HashMap<String, SensorTemp.Callback>();

	public SensorTemp(Context context) {
		this.context = context;
		mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		mTempSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

		mSensorManager.registerListener(listener, mTempSensor, SensorManager.SENSOR_DELAY_UI);
	}

	public void startMonitor(boolean isPeriodic, long rateSec, SensorTemp.Callback cb) {
		this.cb = cb;

		// mSensorManager.registerListener(listener, mTempSensor, SensorManager.SENSOR_DELAY_UI);
	}

	public void startThreshold(int threshold, SensorTemp.Callback cb) {
		this.cb = cb;
	}

	public void close() {
		mSensorManager.unregisterListener(listener);
	}

	public void restartListenner() {
		mSensorManager.unregisterListener(listener);
		mSensorManager.registerListener(listener, mTempSensor, SensorManager.SENSOR_DELAY_UI);
	}

	//
	//
	//
	//
	//

	public interface Callback {
		/**
		 * 
		 * @param temp
		 * @param isUp
		 */
		void tempThresholdReached(int temp, boolean isUp);

		/**
		 * 
		 * @param temp
		 */
		void tempCurrent(int temp);
	}

	private SensorEventListener listener = new SensorEventListener() {
		public void onSensorChanged(SensorEvent e) {

			if (e.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
				System.out.println("Temperature is : " + e.values[0] + " - " + e.timestamp + " - " + e.accuracy);
			}
		}

		public void onAccuracyChanged(Sensor sensor, int accuracy) {
		}
	};

}