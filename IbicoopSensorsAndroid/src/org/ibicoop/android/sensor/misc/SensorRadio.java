package org.ibicoop.android.sensor.misc;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.PowerManager;
import android.telephony.CellLocation;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SignalStrength;
import android.telephony.TelephonyManager;

public class SensorRadio extends IbiSensor implements Runnable {

	// android
	PowerManager.WakeLock lock;
	ConnectivityManager cm;
	TelephonyManager tm;
	MyPhoneStateListener myListener;

	// sensor related
	SensorRadioData sensorData = new SensorRadioData();
	int period = 1;
	boolean doMonitoring = true;
	Thread monitorThread;

	// stats
	long initStartRX;
	long initStartTX;

	//
	// Constructor
	//

	static SensorRadio singleton = null;

	synchronized static public SensorRadio getInstance(Context context, IbiSensorListener listener) {
		if (singleton == null)
			singleton = new SensorRadio(context);

		singleton.start(listener);

		return singleton;
	}

	private SensorRadio(Context context) {
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		lock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "SensorRead");
		lock.acquire();

		cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		initStartRX = TrafficStats.getTotalRxBytes();
		initStartTX = TrafficStats.getTotalTxBytes();
	}

	//
	//
	// IBISENSOR METHODS
	//
	//

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
	 */
	public void start(IbiSensorListener listener) {
		startMonitor();
		super.start(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#close()
	 */
	@Override
	public void stop() throws IbiSensorException {
		stopMonitoring();
		super.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#pause()
	 */
	@Override
	public void pause() throws IbiSensorException {
		super.pause();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#resume()
	 */
	@Override
	public void resume() throws IbiSensorException {
		super.resume();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
	 */
	@Override
	public void changeResolution(IbiSensorResolution resolution) {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#getData()
	 */
	@Override
	public IbiSensorData getData() {
		return sensorData;
	}

	//
	//
	//
	//
	//

	/**
	 * 
	 * @param b
	 * @param i
	 * @param radCallback
	 */
	private void startMonitor() {
		doMonitoring = true;
		monitorThread = new Thread(this);
		monitorThread.start();

		setupListener();
	};

	/**
	 * 
	 */
	private void stopMonitoring() {
		try {
			cancelListener();

			doMonitoring = false;
			monitorThread.interrupt();
			monitorThread = null;

			lock.release();
		} catch (Exception e) {
		}
	}

	private void cancelListener() {
		int telephonyFlags = PhoneStateListener.LISTEN_NONE;
		tm.listen(myListener, telephonyFlags);
	}

	private void setupListener() {
		// always register for telephony callbacks
		myListener = new MyPhoneStateListener();

		int telephonyFlags = 0;
		//
		telephonyFlags += PhoneStateListener.LISTEN_CALL_STATE;
		// not supporte yet ?
		// telephonyFlags += PhoneStateListener.LISTEN_CELL_INFO;
		// Requires ACCESS_COARSE_LOCATION
		// telephonyFlags += PhoneStateListener.LISTEN_CELL_LOCATION;
		telephonyFlags += PhoneStateListener.LISTEN_DATA_ACTIVITY;
		telephonyFlags += PhoneStateListener.LISTEN_DATA_CONNECTION_STATE;
		telephonyFlags += PhoneStateListener.LISTEN_MESSAGE_WAITING_INDICATOR;
		telephonyFlags += PhoneStateListener.LISTEN_SERVICE_STATE;
		telephonyFlags += PhoneStateListener.LISTEN_SIGNAL_STRENGTHS;
		//
		tm.listen(myListener, telephonyFlags);
	}

	//
	//
	//
	//
	//

	private class MyPhoneStateListener extends PhoneStateListener {

		public void onCallForwardingIndicatorChanged(boolean cfi) {
			// Callback invoked when the call-forwarding indicator changes.
		}

		public void onCallStateChanged(int state, String incomingNumber) {
			// Callback invoked when device call state changes.
		}

		// public void onCellInfoChanged(List<CellInfo> cellInfo) {
		// // Callback invoked when a observed cell info has changed, or new cells have been added or removed.
		// }

		public void onCellLocationChanged(CellLocation location) {
			// Callback invoked when device cell location changes.
		}

		public void onDataActivity(int direction) {
			// Callback invoked when data activity state changes.
			switch (direction) {
			case TelephonyManager.DATA_ACTIVITY_IN:
				sensorData.getValue().put(SensorRadioData.DATA_ACTIVITY, "DATA_ACTIVITY_IN");
				break;
			case TelephonyManager.DATA_ACTIVITY_OUT:
				sensorData.getValue().put(SensorRadioData.DATA_ACTIVITY, "DATA_ACTIVITY_OUT");
				break;
			case TelephonyManager.DATA_ACTIVITY_INOUT:
				sensorData.getValue().put(SensorRadioData.DATA_ACTIVITY, "DATA_ACTIVITY_INOUT");
				break;
			case TelephonyManager.DATA_ACTIVITY_NONE:
				sensorData.getValue().put(SensorRadioData.DATA_ACTIVITY, "DATA_ACTIVITY_NONE");
				break;
			case TelephonyManager.DATA_ACTIVITY_DORMANT:
				sensorData.getValue().put(SensorRadioData.DATA_ACTIVITY, "DATA_ACTIVITY_DORMANT");
				break;
			}
		}

		// public void onDataConnectionStateChanged(int state) {
		// // Callback invoked when connection state changes.
		// String value = "";
		// switch (state) {
		// case TelephonyManager.DATA_CONNECTED:
		// value = "DATA_CONNECTED";
		// break;
		// case TelephonyManager.DATA_CONNECTING:
		// value = "DATA_CONNECTING";
		// break;
		// case TelephonyManager.DATA_DISCONNECTED:
		// value = "DATA_DISCONNECTED";
		// break;
		// case TelephonyManager.DATA_SUSPENDED:
		// value = "DATA_SUSPENDED";
		// break;
		// default:
		// }
		// System.out.println("XXX PGR : onDataConnectionStateChanged " + value);
		// }

		public void onDataConnectionStateChanged(int state, int networkType) {
			// Callback invoked when connection state changes.
			switch (state) {
			case TelephonyManager.DATA_CONNECTED:
				sensorData.getValue().put(SensorRadioData.DATA_CONN, "DATA_CONNECTED");
				break;
			case TelephonyManager.DATA_CONNECTING:
				sensorData.getValue().put(SensorRadioData.DATA_CONN, "DATA_CONNECTING");
				break;
			case TelephonyManager.DATA_DISCONNECTED:
				sensorData.getValue().put(SensorRadioData.DATA_CONN, "DATA_DISCONNECTED");
				break;
			case TelephonyManager.DATA_SUSPENDED:
				sensorData.getValue().put(SensorRadioData.DATA_CONN, "DATA_SUSPENDED");
				break;
			}

			switch (networkType) {
			case TelephonyManager.NETWORK_TYPE_EDGE:
			case TelephonyManager.NETWORK_TYPE_GPRS:
				sensorData.getValue().put(SensorRadioData.PHONE_NET_TYPE, "EDGE");
				break;
			case TelephonyManager.NETWORK_TYPE_HSDPA:
			case TelephonyManager.NETWORK_TYPE_UMTS:
			case TelephonyManager.NETWORK_TYPE_LTE:
			case TelephonyManager.NETWORK_TYPE_HSPA:
				sensorData.getValue().put(SensorRadioData.PHONE_NET_TYPE, "3G");
				break;
			default:
				sensorData.getValue().put(SensorRadioData.PHONE_NET_TYPE, "OTHER");
			}
		}

		public void onMessageWaitingIndicatorChanged(boolean mwi) {
			// Callback invoked when the message-waiting indicator changes.
		}

		public void onServiceStateChanged(ServiceState serviceState) {
			// Callback invoked when device service state changes.
		}

		public void onSignalStrengthChanged(int asu) {
			// This method was deprecated in API level 5. Use onSignalStrengthsChanged(SignalStrength)
		}

		public void onSignalStrengthsChanged(SignalStrength signalStrength) {
			// Callback invoked when network signal strengths changes.
			super.onSignalStrengthsChanged(signalStrength);
			int signalStenths = signalStrength.getGsmSignalStrength();

			if (signalStenths > 30) {
				// signalstrength.setText("Signal Str : Good");
				// signalstrength.setTextColor(getResources().getColor(R.color.good));
			} else if (signalStenths > 20 && signalStenths < 30) {
				// signalstrength.setText("Signal Str : Average");
				// signalstrength.setTextColor(getResources().getColor(R.color.average));
			} else if (signalStenths < 20) {
				// signalstrength.setText("Signal Str : Weak");
				// signalstrength.setTextColor(getResources().getColor(R.color.weak));
			}

		}

	}

	//
	//
	//
	//
	//

	@Override
	public void run() {
		while (doMonitoring) {
			try {
				Thread.sleep(period * 1000);
			} catch (InterruptedException e) {
				return;
			}

			// get info on both interfaces
			NetworkInfo mWifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			NetworkInfo mMobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			// System.out.println("XXX PGR WIFI : type=" + mWifi.getTypeName() + " - extra=" + mWifi.getExtraInfo() + " - subtype=" + mWifi.getSubtypeName() + " - avail=" + mWifi.isAvailable()
			// + " - con=" + mWifi.isConnected() + " - c/c=" + mWifi.isConnectedOrConnecting() + " - fo=" + mWifi.isFailover() + " - state=" + mWifi.getState().name());
			// System.out.println("XXX PGR PHONE : type=" + mMobile.getTypeName() + " - extra=" + mMobile.getExtraInfo() + " - subtype=" + mMobile.getSubtypeName() + " - avail=" +
			// mMobile.isAvailable()
			// + " - con=" + mMobile.isConnected() + " - c/c=" + mMobile.isConnectedOrConnecting() + " - fo=" + mMobile.isFailover() + " - state=" + mMobile.getState().name());

			//
			NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
			if (activeNetwork != null) {
				switch (activeNetwork.getType()) {
				case (ConnectivityManager.TYPE_WIFI):
					sensorData.getValue().put(SensorRadioData.NET_TYPE, "WiFi");
					break;
				case (ConnectivityManager.TYPE_MOBILE):
					sensorData.getValue().put(SensorRadioData.NET_TYPE, "Mobile");
					break;
				default:
					sensorData.getValue().put(SensorRadioData.NET_TYPE, "Other");
				}
			} else {
				sensorData.getValue().put(SensorRadioData.NET_TYPE, "ERROR");
			}

			long tmp = TrafficStats.getTotalRxBytes();

			long currentRX = tmp - initStartRX;
			initStartRX = tmp;
			sensorData.getValue().put(SensorRadioData.CURRENT_RX, "" + currentRX);

			tmp = TrafficStats.getTotalTxBytes();
			long currentTX = tmp - initStartTX;
			initStartTX = tmp;
			sensorData.getValue().put(SensorRadioData.CURRENT_TX, "" + currentTX);
		}
	}

}
