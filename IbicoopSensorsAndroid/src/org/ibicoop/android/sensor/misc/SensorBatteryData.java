package org.ibicoop.android.sensor.misc;

import java.util.HashMap;

import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

public class SensorBatteryData extends IbiSensorData {

	public final static String CHARGING = "Charging";
	public final static String TEMP = "Temp";
	public final static String PERCENT = "Percent";

	HashMap<String, String> sensorsData = new HashMap();

	@Override
	public HashMap<String, String> getValue() {
		return sensorsData;
	}

	@Override
	public void copy(IbiSensorData dst) {
		super.copy(dst);
		SensorBatteryData localDst = (SensorBatteryData) dst;
		localDst.sensorsData = new HashMap(sensorsData);
	}

	@Override
	public String toString() {
		return sensorsData.toString();
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {
		// TODO Auto-generated method stub
		return false;
	}

}
