package org.ibicoop.android.sensor.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorProperties;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocation;
import org.ibicoop.sensor.resolution.IbiSensorResolutionLocationAccuracyType;
import org.ibicoop.sensor.unit.IbiSensorUnitLocation;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;

@SuppressLint("InlinedApi")
public class IbiAndroidSensorLocation extends IbiSensor implements LocationListener {

		private static final Boolean DEBUG = false;
	
        private static final String NAME = "Location sensor";
        private static final String PROXIMITY_ALERT_INTENT = "Ibicoop.LocationSensor.ProximityAlert";
        private static final String ID = "Id";
        private static final String LATITUDE = "Latitude";
        private static final String LONGITUDE = "Longitude";
        private static final String ALTITUDE = "Altitude";
        private static final String BEARING = "Bearing";
        private static final String SPEED = "Speed";
        private static final String RADIUS = "Radius";
        private static final String TIME = "Time";

        private static final HashMap<IbiSensorResolutionLocationAccuracyType, Integer> ANDROID_ACCURACY_MAP;

        static {
                ANDROID_ACCURACY_MAP = new HashMap<IbiSensorResolutionLocationAccuracyType, Integer>();
                ANDROID_ACCURACY_MAP.put(IbiSensorResolutionLocationAccuracyType.ACCURACY_COARSE, Criteria.ACCURACY_COARSE);
                ANDROID_ACCURACY_MAP.put(IbiSensorResolutionLocationAccuracyType.ACCURACY_FINE, Criteria.ACCURACY_FINE);
        }

        // Android
        private Context context;
        private LocationManager locationManager;
        private String provider;
        private LocationProvider locationProvider;
        private Criteria criteria;
        private BroadcastReceiver broadcastReceiver;

        // Ibicoop
        private IbiSensorResolutionLocation ibiSensorResolutionLocation;
        private IbiSensorDataLocation ibiSensorData;
        private IbiAndroidSensorDataInRegionSystemTrigger systemRegionMonitoringTrigger;

        private int requestCode = 0;
        private List<PendingIntent> pendingIntentList;
        private HashMap<IbiSensorDataLocation, PendingIntent> pendingIntentMap;

        private boolean isStarted = false;;

        //
        //
        //
        //
        //
        private static IbiAndroidSensorLocation singleton;

        private IbiAndroidSensorLocation(Context paramContext) {
                context = paramContext;

                // init
                locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                criteria = new Criteria();
                
                ibiSensorData = new IbiSensorDataLocation(0.0, 0.0, 0.0, 0.0f, 0.0f, System.currentTimeMillis(), 0.0f, null, new IbiSensorUnitLocation());
                pendingIntentList = new ArrayList<PendingIntent>();
                pendingIntentMap = new HashMap<IbiSensorDataLocation, PendingIntent>();
        }

        synchronized public static IbiAndroidSensorLocation getInstance(Context paramContext, IbiSensorResolution paramResolution, IbiSensorListener ibiSensorListener) {
                if (singleton == null) {
                        singleton = new IbiAndroidSensorLocation(paramContext);
                }

                // set the resolution
                singleton.ibiSensorResolutionLocation = ((IbiSensorResolutionLocation) paramResolution);
                //
                singleton.start(ibiSensorListener);

                return singleton;
        }

        //
        //
        //
        //
        //

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
         */
        @Override
        public void start(IbiSensorListener ibiSensorListener) {
                criteria.setAccuracy(ANDROID_ACCURACY_MAP.get(ibiSensorResolutionLocation.getAccuracy()));
                
                if (Build.VERSION.SDK_INT >= 9) {
                	
                	if (ibiSensorResolutionLocation.getAccuracy() == IbiSensorResolutionLocationAccuracyType.ACCURACY_FINE) {
                		criteria.setHorizontalAccuracy(Criteria.ACCURACY_HIGH);
                	} else {
                		criteria.setHorizontalAccuracy(Criteria.ACCURACY_MEDIUM);
                	}
                }
                
                provider = locationManager.getBestProvider(criteria, true);
              
                if (DEBUG) System.out.println("Provider = " + provider);
                
                locationProvider = locationManager.getProvider(provider);
                
                if (DEBUG) System.out.println("Location provider is " + locationProvider.getName());
                       
                Location initLocation = locationManager.getLastKnownLocation(locationProvider.getName());
                
                if (initLocation != null) {
                    double initLatitude = initLocation.getLatitude();
                    double initLongitude = initLocation.getLongitude();
                    if (DEBUG) System.out.println("***init latitude = " + initLatitude + ", longitude = " + initLongitude + "***");
                    ibiSensorData = new IbiSensorDataLocation(initLatitude, initLongitude, 0.0, 0.0f, 0.0f, System.currentTimeMillis(), 0.0f, null, new IbiSensorUnitLocation());
                }

             
                try {
                        super.setProperties(new IbiSensorProperties(NAME, locationProvider.getName()));
                } catch (IbiSensorException e) {
                }

                if (!isStarted) {
                        initialiseReceiver();
                        registerListener();
                        isStarted = true;
                }

                super.start(ibiSensorListener);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
         */
        @Override
        public void changeResolution(IbiSensorResolution resolution) {
                ibiSensorResolutionLocation = ((IbiSensorResolutionLocation) resolution);
                // start();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#stop()
         */
        @Override
        public void stop() throws IbiSensorException {
                if (isStarted) {

                        for (PendingIntent intent : pendingIntentList) {
                                locationManager.removeProximityAlert(intent);
                        }
                        
                        //Clear proximity aler pending intent list and map
                        pendingIntentList.clear();
                        pendingIntentMap.clear();

                        unregisterListener();

                        if (broadcastReceiver != null)
                                context.unregisterReceiver(broadcastReceiver);

                        isStarted = false;
                }

                super.stop();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#getData()
         */
        @Override
        public IbiSensorData getData() {
                return ibiSensorData;
        }

        //
        //
        //

        /*
         * (non-Javadoc)
         * 
         * @see android.location.LocationListener#onLocationChanged(android.location.Location)
         */
        @Override
        public void onLocationChanged(Location eventLocation) {

        	if (DEBUG) System.out.println("On Location changed: lat = " + eventLocation.getLatitude() + ", lon = " + eventLocation.getLongitude());
        	
                if (eventLocation.getProvider().equals(locationProvider.getName())) {
                		System.out.println("Check: On Location changed: lat = " + eventLocation.getLatitude() + ", lon = " + eventLocation.getLongitude());
                		
                        ibiSensorData = new IbiSensorDataLocation(eventLocation.getLatitude(), eventLocation.getLongitude(), eventLocation.getAltitude(), eventLocation.getBearing(), eventLocation.getSpeed(),
                        // eventLocation.getTime(),
                                        System.currentTimeMillis(), null, new IbiSensorUnitLocation());
                }
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.location.LocationListener#onProviderDisabled(java.lang.String)
         */
        @Override
        public void onProviderDisabled(String eventProvider) {

                if (provider.equals(eventProvider)) {
                        try {
                                // XXX TODO not sure that's the proper message to notify ??
                                super.pause();
                        } catch (IbiSensorException e) {
                        }
                }
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.location.LocationListener#onProviderEnabled(java.lang.String)
         */
        @Override
        public void onProviderEnabled(String eventProvider) {

                if (provider.equals(eventProvider))
                        try {
                                // XXX TODO not sure that's the proper message to notify ??
                                super.resume();
                        } catch (IbiSensorException e) {
                        }
        }

        /*
         * (non-Javadoc)
         * 
         * @see android.location.LocationListener#onStatusChanged(java.lang.String, int, android.os.Bundle)
         */
        @Override
        public void onStatusChanged(String eventProvider, int status, Bundle extras) {

                try {
                        if (provider.equals(eventProvider)) {
                                if ((status == LocationProvider.OUT_OF_SERVICE) || (status == LocationProvider.TEMPORARILY_UNAVAILABLE)) {
                                        // XXX TODO not sure that's the proper message to notify ??
                                        super.pause();
                                } else if (status == LocationProvider.AVAILABLE) {
                                        // XXX TODO not sure that's the proper message to notify ??
                                        super.resume();
                                }
                        }
                } catch (IbiSensorException e) {
                }
        }

        //
        //
        //
        //
        //

        public void setSystemRegionMonitoringTrigger(IbiAndroidSensorDataInRegionSystemTrigger trigger) {
                systemRegionMonitoringTrigger = trigger;
        }

        public void addSystemRegionMonitoringTriggerThreshold(IbiSensorDataLocation threshold, long alertActiveTimeMs) {
                addProximityAlert(threshold, alertActiveTimeMs);
        }
        
        public void removeSystemRegionMonitoringTriggerThreshold(IbiSensorDataLocation threshold) {
            	removeProximityAlert(threshold);
        } 

        //
        //
        //
        //
        //
        /**
         * Add stops coordinate to android location sensor proximity alert system
         * @param locationData
         * @param alertActiveTimeMs
         */
        private void addProximityAlert(IbiSensorDataLocation locationData, long alertActiveTimeMs) {

                Bundle extras = new Bundle();
                extras.putInt(ID, requestCode);

                Intent intent = new Intent(PROXIMITY_ALERT_INTENT);
                intent.putExtra(PROXIMITY_ALERT_INTENT, extras);
                intent.putExtra(LATITUDE, locationData.getLatitude());
                intent.putExtra(LONGITUDE, locationData.getLongitude());
                intent.putExtra(ALTITUDE, locationData.getAltitude());
                intent.putExtra(BEARING, locationData.getBearing());
                intent.putExtra(SPEED, locationData.getSpeed());
                intent.putExtra(RADIUS, locationData.getRadius());
                intent.putExtra(TIME, locationData.getTimeStamp());

                Map<String, String> extraParameters = locationData.getExtraParameters();
                Set<String> paramKeys = extraParameters.keySet();

                for (String key : paramKeys) {           	
                   intent.putExtra(key, (String) extraParameters.get(key));
                }

                PendingIntent proximityIntent = PendingIntent.getBroadcast(context, requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT);

                //Add the proximity alert pending intent into the list and map
                pendingIntentList.add(proximityIntent);
                pendingIntentMap.put(locationData, proximityIntent);
                
                if (DEBUG) System.out.println("AddProximityAlert: lat = " +  locationData.getLatitude() + ", " +
                							"lon = " + locationData.getLongitude() + ", " + 
                							"radius = " + locationData.getRadius() + ", " + 
                							"alertActiveTime = " + alertActiveTimeMs + " ms"
                							); 

                locationManager.addProximityAlert(locationData.getLatitude(), // the latitude of the central point of the alert region
                                locationData.getLongitude(), // the longitude of the central point of the alert region
                                locationData.getRadius(), // the radius of the central point of the alert region, in meters
                                alertActiveTimeMs,// time for this proximity alert, in milliseconds, or -1 to indicate no expiration
                                proximityIntent // will be used to generate an Intent to fire when entry to or exit from the alert region is detected
                                );

                requestCode++;
        }
        
        /**
         * Remove stops coordinate from android location sensor proximity alert system
         * @param locationData
         */
        private void removeProximityAlert(IbiSensorDataLocation locationData) {
        	if (DEBUG) System.out.println("Remove proximity alert! data = " + locationData.toString());
        	
        	if (!pendingIntentMap.containsKey(locationData)) return;
        	PendingIntent intent = pendingIntentMap.get(locationData);
        	
        	//Remove alert from location manager
            locationManager.removeProximityAlert(intent);

		    //Clear proximity aler pending intent list and map
		    pendingIntentList.remove(intent);
		    pendingIntentMap.remove(locationData);
		    
		    if (DEBUG) System.out.println("Remove proximity alert ok!");
        }

        private void initialiseReceiver() {
                IntentFilter alertIntentFilter = new IntentFilter(PROXIMITY_ALERT_INTENT);

                broadcastReceiver = new BroadcastReceiver() {

                        @Override
                        public void onReceive(Context paramContext, Intent paramIntent) {

                                if (paramIntent.getAction().equals(PROXIMITY_ALERT_INTENT)) {

                                        String key = LocationManager.KEY_PROXIMITY_ENTERING;
                                        boolean entering = paramIntent.getBooleanExtra(key, false);
                                        
                                        if (DEBUG) System.out.println("Proximity entering key =" + key + ", entering value = " + entering);

                                        double triggerLatitude = paramIntent.getDoubleExtra(LATITUDE, -1);
                                        double triggerLongitude = paramIntent.getDoubleExtra(LONGITUDE, -1);
                                        double triggerAltitude = paramIntent.getDoubleExtra(ALTITUDE, -1);
                                        float triggerRadius = paramIntent.getFloatExtra(RADIUS, -1);
                                        float triggerBearing = paramIntent.getFloatExtra(BEARING, -1);
                                        float triggerSpeed = paramIntent.getFloatExtra(SPEED, -1);
                                        long triggerTime = paramIntent.getLongExtra(TIME, -1);

                                        Bundle bundle = paramIntent.getExtras();
                                        Map<String, String> extraParameters = new HashMap<String, String>();

                                        if (bundle != null) {
                                        	if (DEBUG) System.out.println("Bundle is not null");
                                                Set<String> keys = bundle.keySet();

                                                Iterator<String> it = keys.iterator();
                                                while (it.hasNext()) {

                                                        String bundleKey = it.next();
                                                        // Assure that bundle key and its value are both String
                                                        if (bundle.get(bundleKey).getClass().equals(bundleKey.getClass())) {
                                                                extraParameters.put(bundleKey, (String) bundle.get(bundleKey));
                                                        }
                                                }
                                                
                                                IbiSensorDataLocation ibiSensorInRegionData = new IbiSensorDataLocation(triggerLatitude, triggerLongitude, triggerAltitude, triggerBearing, triggerSpeed, triggerTime,
                                                        triggerRadius, extraParameters, new IbiSensorUnitLocation());

                                                systemRegionMonitoringTrigger.receiveSystemCallback(entering, ibiSensorInRegionData);
                                        }
                                 }
                      }
                };

                context.registerReceiver(broadcastReceiver, alertIntentFilter);
        }

        private void registerListener() {
        	if (DEBUG) System.out.println("Request location updates: " + locationProvider.getName());
                locationManager.requestLocationUpdates(locationProvider.getName(), ibiSensorResolutionLocation.getTime(), ibiSensorResolutionLocation.getDistance(), singleton);
        }

        private void unregisterListener() {
                locationManager.removeUpdates(singleton);
        }

}