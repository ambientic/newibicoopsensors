package org.ibicoop.android.sensor.impl;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorFactory;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorProperties;
import org.ibicoop.sensor.IbiSensorType;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataBoolean;
import org.ibicoop.sensor.data.IbiSensorDataFloat3D;
import org.ibicoop.sensor.data.IbiSensorDataMicrophone;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;
import org.ibicoop.sensor.resolution.IbiSensorResolutionMicrophone;
import org.ibicoop.sensor.resolution.IbiSensorResolutionTime;
import org.ibicoop.sensor.trigger.IbiSensorDataTimerNotAdaptativeNewDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;
import org.ibicoop.sensor.unit.IbiSensorUnit;
import org.ibicoop.sensor.unit.IbiSensorUnitType;
import org.ibicoop.android.statespace.MetroModel;

import android.content.Context;

/**
 * Metro sensor to detect is metro is moving or stopping It uses acceleration and sound amplitude data
 * 
 * @author khoo
 * 
 */
public class IbiAndroidSensorMetro extends IbiSensor {

        private static final String NAME = "Metro";
        private static final String MODEL = "Metro dynamic system";

        private static final int PERIOD = 1000; // 1 second
        private static final int SAMPLING_RATE = 44100; // 44.1 kHz
        private static final int ACCELERATION_RESOLUTION = 20; // 20 ms

        // Android
        private Context context;

        // Ibicoop
        private IbiSensorDataBoolean ibiMetroSensorData;

        // Ibicoop sensors: accelerometer and soundSensor
        private IbiSensor accelerometer;
        private IbiSensor soundSensor;

        // Ibicoop sensor data trigger
        private IbiSensorDataTimerNotAdaptativeNewDataTrigger newDataTrigger;
        private IbiAndroidSensorDataMetroStopTrigger stopTrigger;

        private boolean isStarted = false;

        // For metro model
        private MetroModel model;
        private float[] accelerations = { 0f, 0f, 0f };
        private double amplitude = 0.0;
        private double decibel = 0.0;
        private float frequency = 0f;

        // Stop and move count
        // Duration for one count is one second
        private int moveCount = 0;
        private int stopCount = 0;

        //
        //
        //
        //
        //

        private static IbiAndroidSensorMetro singleton;

        private IbiAndroidSensorMetro(Context paramContext) {
                context = paramContext;
                ibiMetroSensorData = new IbiSensorDataBoolean(false, System.currentTimeMillis());
                model = new MetroModel();
        }

        synchronized public static IbiAndroidSensorMetro getInstance(Context paramContext, IbiSensorListener ibiSensorListener) {
                if (singleton == null) {
                        singleton = new IbiAndroidSensorMetro(paramContext);
                }

                try {
                        singleton.setProperties(new IbiSensorProperties(NAME, MODEL));
                } catch (IbiSensorException e) {
                }
                singleton.start(ibiSensorListener);

                return singleton;
        }

        //
        //
        //
        //
        //

        /**
         * Set metro stop trigger
         * 
         * @param stopTrigger
         */
        public void setStopTrigger(IbiAndroidSensorDataMetroStopTrigger newStopTrigger) {
                stopTrigger = newStopTrigger;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
         */
        @Override
        public void start(IbiSensorListener ibiSensorListener) {
                System.out.println("Start metro sensor");

                if (!isStarted) {
                        // Start sound sensor
                        try {
                                soundSensor = IbiSensorFactory.getInstance().getSensor(context, IbiSensorType.MICROPHONE, new IbiSensorResolutionMicrophone(SAMPLING_RATE, PERIOD), ibiSensorListener);

                                // Start accelerometer
                                accelerometer = IbiSensorFactory.getInstance().getSensor(context, IbiSensorType.ACCELEROMETER, new IbiSensorResolutionTime(ACCELERATION_RESOLUTION), ibiSensorListener);
                        } catch (IbiSensorException e) {
                        }

                        // Start new data trigger
                        IbiSensorData initThreshold = new IbiSensorDataFloat3D(0f, 0f, 0f, 0l, new IbiSensorUnit(IbiSensorUnitType.ACCELERATION_UNIT));
                        newDataTrigger = new IbiSensorDataTimerNotAdaptativeNewDataTrigger(accelerometer, initThreshold, triggerListener, PERIOD, -1);
                }

                isStarted = true;

                super.start(ibiSensorListener);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#getData()
         */
        @Override
        public IbiSensorData getData() {
                return ibiMetroSensorData;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#stop()
         */
        @Override
        public void stop() throws IbiSensorException {
                System.out.println("Stop metro sensor");
                stopSensors();
                super.stop();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#pause()
         */
        @Override
        public void pause() throws IbiSensorException {
                // TODO Auto-generated method stub

        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#resume()
         */
        @Override
        public void resume() throws IbiSensorException {
                // TODO Auto-generated method stub

        }

        //
        //
        //
        //
        //

        private IbiSensorDataTriggerListener triggerListener = new IbiSensorDataTriggerListener() {

                @Override
                public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {

                        accelerations = (float[]) ibiSensorData.getValue();

                        // Get microphone sound data
                        IbiSensorDataMicrophone microphoneData;
                        try {
                                microphoneData = (IbiSensorDataMicrophone) soundSensor.getData();
                                amplitude = microphoneData.getAmplitude();
                                decibel = microphoneData.getDecibel();
                                frequency = microphoneData.getFrequency();

                                // Update model state
                                model.updateState(accelerations, amplitude, frequency);

                                // Update ibisensor data
                                boolean movement = false;

                                if (model.getProcessedOutput() == 1) {
                                        movement = true;
                                        // Increment move count and reset stop count
                                        moveCount++;
                                        stopCount = 0;
                                } else {
                                        // Increment stop count and reset move count
                                        stopCount++;
                                        moveCount = 0;
                                }

                                ibiMetroSensorData = new IbiSensorDataBoolean(movement, System.currentTimeMillis());

                                if ((movement == false) && (moveCount > stopTrigger.getStopMoreThanSecond())) {
                                        // Metro stop, trigger
                                        if (stopTrigger != null)
                                                stopTrigger.receiveSystemCallback(ibiMetroSensorData);
                                }
                        } catch (IbiSensorException e) {
                        }

                }
        };

        //
        //
        //
        //
        //

        public void stopSensors() throws IbiSensorException {

                if (isStarted) {
                        System.out.println("Stop all sensors");
                        // Stop new data trigger
                        if (newDataTrigger != null)
                                newDataTrigger.stop();

                        // Stop accelerometer
                        if (accelerometer != null)
                                accelerometer.stop();

                        // Stop sound sensor
                        if (soundSensor != null)
                                soundSensor.stop();

                        // Reset model state
                        if (model != null)
                                model.resetState();

                        isStarted = false;

                        // Reset move and stop count
                        moveCount = 0;
                        stopCount = 0;
                }
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
         */
        @Override
        public void changeResolution(IbiSensorResolution resolution) throws IbiSensorException {
                // TODO Auto-generated method stub

        }
}