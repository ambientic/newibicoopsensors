package org.ibicoop.android.sensor.impl;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorProperties;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataFloat3D;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;
import org.ibicoop.sensor.resolution.IbiSensorResolutionTime;
import org.ibicoop.sensor.unit.IbiSensorUnit;
import org.ibicoop.sensor.unit.IbiSensorUnitType;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;

/**
 * Ibicoop accelerometer
 * 
 * @author khoo
 * 
 */
public class IbiAndroidSensorAccelerometer extends IbiSensor implements SensorEventListener {

	private static final String NAME = "Accelerometer";

	// Android
	private Context context;
	private SensorManager sensorManager;
	private Sensor accelerometer;
	private BroadcastReceiver broadcastReceiver;
	private IntentFilter intentFilter; // To filter screen on off

	// Ibicoop
	private IbiSensorDataFloat3D ibiSensorData;
	private IbiSensorResolutionTime ibiSensorResolutionTime;
	private boolean isStarted = false;
	private int time;

	//
	//
	//

	private static IbiAndroidSensorAccelerometer singleton;

	private IbiAndroidSensorAccelerometer(Context paramContext) {
		context = paramContext;
		sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
		accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		try {
			IbiSensorProperties ibiSensorProperties = new IbiSensorProperties(NAME, accelerometer.getName());
			super.setProperties(ibiSensorProperties);
		} catch (IbiSensorException e) {
		}
		ibiSensorData = new IbiSensorDataFloat3D(0f, 0f, 0f, System.currentTimeMillis(), new IbiSensorUnit(IbiSensorUnitType.ACCELERATION_UNIT));
		createFilter();
		createReceiver();
	}

	synchronized public static IbiAndroidSensorAccelerometer getInstance(Context paramContext, IbiSensorResolution paramResolution, IbiSensorListener ibiSensorListener) {

		if (singleton == null) {
			singleton = new IbiAndroidSensorAccelerometer(paramContext);
		}

		// Must be a time resolution
		singleton.ibiSensorResolutionTime = ((IbiSensorResolutionTime) paramResolution);

		// start the platform sensor and then register/call the listener to notify started
		singleton.start(ibiSensorListener);

		return singleton;
	}

	//
	//
	//
	//
	//

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
	 */
	@Override
	public void start(IbiSensorListener listener) {
		//
		restart();

		//
		super.start(listener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#stop()
	 */
	@Override
	public void stop() throws IbiSensorException {

		if (isStarted) {
			// Stop the sensor only if the sensor had started
			unregisterPlatformListener();
			context.unregisterReceiver(broadcastReceiver);
			isStarted = false;
		}

		// will notify all the listeners
		super.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#getData()
	 */
	@Override
	public IbiSensorData getData() {
		return ibiSensorData;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
	 */
	@Override
	public void changeResolution(IbiSensorResolution resolution) {

		// Must be a time resolution
		ibiSensorResolutionTime = ((IbiSensorResolutionTime) resolution);

		restart();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.hardware.SensorEventListener#onAccuracyChanged(android.hardware.Sensor, int)
	 */
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {
		// NOT USED in our case
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.hardware.SensorEventListener#onSensorChanged(android.hardware.SensorEvent)
	 */
	@Override
	public void onSensorChanged(SensorEvent event) {

		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			float values[] = event.values;
			// ibiSensorData = new IbiSensorDataFloat3D(values[0], values[1], values[2], event.timestamp, new IbiSensorUnit(IbiSensorUnitType.ACCELERATION_UNIT));
			ibiSensorData = new IbiSensorDataFloat3D(values[0], values[1], values[2], System.currentTimeMillis(), new IbiSensorUnit(IbiSensorUnitType.ACCELERATION_UNIT));
		}
	}

	//
	//
	//
	//
	//

	private void createFilter() {
		intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
	}

	private void createReceiver() {

		broadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context paramContext, Intent paramIntent) {

				if (paramIntent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {

					System.out.println("Received screen off event!");

					Runnable runnable = new Runnable() {
						@Override
						public void run() {
							System.out.println("Going to unregister and register event listener!");
							unregisterPlatformListener();
							registerPlatformListener();
						}
					};

					new Handler().postDelayed(runnable, 100);
				}
			}
		};
	}

	private void restart() {

		System.out.println("Restart accelerometer");

		time = (int) (((Long) ibiSensorResolutionTime.getValue()) * 1000); // Convert to microsecond

		if (!isStarted) {
			context.registerReceiver(broadcastReceiver, intentFilter);
			isStarted = true;
		} else {
			// The sensor is already started, we need to stop the sensor before restarting it
			unregisterPlatformListener();
		}

		// Start or restart the sensor
		registerPlatformListener();
	}

	private void registerPlatformListener() {
		sensorManager.registerListener(singleton, accelerometer, time);
	}

	private void unregisterPlatformListener() {
		sensorManager.unregisterListener(singleton);
	}
}
