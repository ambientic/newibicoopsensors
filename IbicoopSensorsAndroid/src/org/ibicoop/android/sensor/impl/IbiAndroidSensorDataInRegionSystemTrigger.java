package org.ibicoop.android.sensor.impl;

import java.util.ArrayList;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataLocation;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataInRegionSystemTriggerListener;

/**
 * Implementation of IbiSensorDataInRegionSystemTrigger in Android
 * @author khoo
 *
 */
public class IbiAndroidSensorDataInRegionSystemTrigger extends IbiSensorDataInRegionSystemTrigger {

	public IbiAndroidSensorDataInRegionSystemTrigger() {}
	
	@Override
	public void init(IbiSensor ibiSensor, long alertActiveTime, IbiSensorDataInRegionSystemTriggerListener listener) {
		super.ibiSensor = ibiSensor;
		super.alertActiveTimeMs = alertActiveTime;
		super.triggerListener = listener;
		super.thresholds = new ArrayList<IbiSensorDataLocation>();
		((IbiAndroidSensorLocation) super.ibiSensor).setSystemRegionMonitoringTrigger(this);
	}
		
	@Override
	public void callSystem(IbiSensorDataLocation threshold, boolean add) {
		if (ibiSensor != null) {
			if (add) ((IbiAndroidSensorLocation) ibiSensor).addSystemRegionMonitoringTriggerThreshold(threshold, alertActiveTimeMs);
		}
	}	
	
	@Override
	public void receiveSystemCallback(boolean inOrOut, IbiSensorDataLocation callbackData) {
		if (triggerListener != null) {
			if (inOrOut) {
				//In
				triggerListener.ibiSensorDataInRegionTrigger(ibiSensor, this, callbackData);
			} else {
				//Out
				triggerListener.ibiSensorDataOutOfRegionTrigger(ibiSensor, this, callbackData);				
			}
		}
	}


}