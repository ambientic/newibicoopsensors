package org.ibicoop.android.sensor.impl;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.IbiSensorListener;
import org.ibicoop.sensor.IbiSensorProperties;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataMicrophone;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;
import org.ibicoop.sensor.resolution.IbiSensorResolutionMicrophone;
import org.ibicoop.sensor.unit.IbiSensorUnitMicrophone;

import edu.emory.mathcs.jtransforms.fft.DoubleFFT_1D;

import android.content.Context;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder.AudioSource;

public class IbiAndroidSensorMicrophone extends IbiSensor {

		private static final boolean DEBUG = false;
		private static final String NAME = "Microphone";
        private static final String MODEL = "Microphone with PCM 16 bit encoding";

        // Android
        private Context context;

        // Ibicoop
        private IbiSensorDataMicrophone ibiSensorData;
        private IbiSensorResolutionMicrophone ibiSensorResolutionMicrophone;

        // Microphone configuration
        // Reference : Professional Android Sensor Programming
        private final int MAXIMUM_AMPLITUDE = 32767;
        // AudioRecord allows to collect the raw and uncompressed audio bytes, which
        // contain recorded samples of the signal's amplitude over time
        private AudioRecord audioRecord;
        // 44100 Hz is the only value guaranteed to work on all devices according to
        // Android documentation. But we will let user set this sampling rate if necessary
        private int samplingRate = 44100; // Hz
        // Number of samples needed
        private int numberSamplesNeeded = 0;

        // Increase the size of the recording buffer by a factor to avoid buffer overflow
        private int bufferIncreasedFactor = 3;

        // The recording and read buffer size
        private int minimumBufferSize = 0;

        // The required recording buffer size
        private int requiredRecordingBufferSize = 0;

        // The increased recording buffer size to avoid overflow
        private int increasedRecordingBufferSize = 0;

        // The required read buffer size
        private int requiredReadBufferSize = 0;

        // Type of encoding which specifies the size of each audio data byte
        private int encoding = AudioFormat.ENCODING_PCM_16BIT;

        // ENCODING_PCM_16BIT : each sample uses 2 bytes
        // ENCODING_PCM_8BIT : each sample uses 1 byte
        private int numberBytesPerSample = 0;

        // Define the audio channels to record
        // CHANNEL_IN_MONO is guaranteed to work on all devices according to Android documentation
        private int channel = AudioFormat.CHANNEL_IN_MONO;

        // Audio data
        private short[] audioData;

        // Recording period
        private long period = 0;

        // Periodic thread to read audio data
        private final int CORE_POOL_SIZE = 2;
        private ScheduledThreadPoolExecutor threadExecutor;
        private ReadAudioThread readAudioThread;

        private boolean isStarted = false;

        private double amplitude = 0.0;
    	private double maxAmplitude = 0.0;
    	private double minAmplitude = 0.0;
    	private double meanAmplitude = 0.0;
    	private double varianceAmplitude = 0.0;
    	private double standardDeviationAmplitude = 0.0;
    	private double range = 0.0;
        private double decibel = 0.0;
        private float frequency = 0f;

        //
        //
        //
        //
        //

        private static IbiAndroidSensorMicrophone singleton;

        private IbiAndroidSensorMicrophone(Context paramContext) {
                context = paramContext;
                threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
                readAudioThread = new ReadAudioThread();
                ibiSensorData = new IbiSensorDataMicrophone(amplitude, decibel, frequency, System.currentTimeMillis(), new IbiSensorUnitMicrophone());
                try {
                        this.setProperties(new IbiSensorProperties(NAME, MODEL));
                } catch (IbiSensorException e) {
                }
        }

        synchronized public static IbiAndroidSensorMicrophone getInstance(Context paramContext, IbiSensorResolution paramResolution, IbiSensorListener paramListener) {
                if (singleton == null) {
                        singleton = new IbiAndroidSensorMicrophone(paramContext);
                }

                // Must be a microphone resolution
                singleton.ibiSensorResolutionMicrophone = ((IbiSensorResolutionMicrophone) paramResolution);
                //
                singleton.start(paramListener);

                return singleton;
        }

        //
        //
        //
        //
        //

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#start(org.ibicoop.sensor.IbiSensorListener)
         */
        public void start(IbiSensorListener ibiSensorListener) {

                // Get recording period in millisecond
                period = ibiSensorResolutionMicrophone.getRecordingPeriod();
                // Get sampling rate in Hz
                samplingRate = ibiSensorResolutionMicrophone.getSamplingRate();

                stopRecordingAudio();
                preparingAudioRecord();
                startRecordingAudio();

                // Start periodic audio processing thread
                if (!isStarted) {
                        threadExecutor.scheduleAtFixedRate(readAudioThread, 0, period, TimeUnit.MILLISECONDS);
                        isStarted = true;
                }

                super.start(ibiSensorListener);
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#stop()
         */
        @Override
        public void stop() throws IbiSensorException {
                stopRecordingAudio();
                super.stop();
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#getData()
         */
        @Override
        public IbiSensorData getData() {
                return ibiSensorData;
        }

        /*
         * (non-Javadoc)
         * 
         * @see org.ibicoop.sensor.IbiSensor#changeResolution(org.ibicoop.sensor.resolution.IbiSensorResolution)
         */
        @Override
        public void changeResolution(IbiSensorResolution resolution) {
                // Must be a microphone resolution
                ibiSensorResolutionMicrophone = ((IbiSensorResolutionMicrophone) resolution);
        }

        //
        //
        //
        //
        //

        /**
         * Set the necessary configuration for audio recording
         */
        private void preparingAudioRecord() {
                if (DEBUG) System.out.println("Prepare audio recording");

                if (audioRecord == null) {
                        minimumBufferSize = AudioRecord.getMinBufferSize(samplingRate, channel, encoding) * 3;

                        if (encoding == AudioFormat.ENCODING_PCM_16BIT) {
                                numberBytesPerSample = 2;
                        } else {
                                numberBytesPerSample = 1;
                        }

                        numberSamplesNeeded = (int) (samplingRate * period / 1000f);
                        requiredReadBufferSize = numberSamplesNeeded;

                        requiredRecordingBufferSize = numberSamplesNeeded * numberBytesPerSample;

                        if (requiredRecordingBufferSize < minimumBufferSize) {
                                requiredRecordingBufferSize = minimumBufferSize;
                        }

                        if (requiredRecordingBufferSize == AudioRecord.ERROR_BAD_VALUE) {
                                if (DEBUG) System.err.println("Invalid value of requiredRecordingBufferSize");
                        }

                        if (requiredRecordingBufferSize == AudioRecord.ERROR) {
                                if (DEBUG) System.err.println("Error creating requiredRecordingBufferSize");
                        }

                        // Increase recording buffer size to avoid buffer overflow
                        increasedRecordingBufferSize = requiredRecordingBufferSize * bufferIncreasedFactor;

                        audioRecord = new AudioRecord(AudioSource.MIC, samplingRate, channel, encoding, increasedRecordingBufferSize);

                        audioData = new short[requiredReadBufferSize];
                }

        }

        /**
         * Start audio recording
         */
        private void startRecordingAudio() {
                try {
                        if (audioRecord != null)
                                audioRecord.startRecording();
                } catch (IllegalStateException illegalStateException) {
                        if (DEBUG) System.err.println(illegalStateException.getMessage());
                }
        }

        /**
         * Processing audio
         */
        private void processingAudio() {
    		computeAllSoundValues();
    	
            ibiSensorData = new IbiSensorDataMicrophone(
            		amplitude,
            		maxAmplitude,
            		minAmplitude,
            		meanAmplitude,
            		varianceAmplitude,
            		standardDeviationAmplitude,
            		range,
            		decibel, 
            		frequency,
            		System.currentTimeMillis(), 
            		new IbiSensorUnitMicrophone());
        }

        private void computeAllSoundValues() {
            // rootMeanSquared for amplitude 
            double rootMeanSquared = 0;
            //Max amplitude
            double maxAmp = Double.MIN_VALUE;
            //Min amplitude
            double minAmp = Double.MAX_VALUE;
            //Mean amplitude
            double sumAmp = 0.0;
            //For calculating number zero crossing
            //int numberZeroCrossing = 0;
        	//Copy audio data
            short[] copyAudioData = new short[audioData.length];
            double[] doubleAudioDataReal = new double[audioData.length];
 
            for (int i = 0; i < audioData.length; i++) {            	
                rootMeanSquared += audioData[i] * audioData[i];
                sumAmp += audioData[i];
                
                if (maxAmp < audioData[i]) {
                	maxAmp = audioData[i];
                }
                
                if (minAmp > audioData[i]) {
                	minAmp = audioData[i];
                }

                /*
                if (i < (audioData.length - 1)) {
                	
                    if ((audioData[i] > 0 && audioData[i + 1] <= 0) || (audioData[i] < 0 && audioData[i + 1] >= 0)) {
                        numberZeroCrossing++;
                    }
                }*/
                
                copyAudioData[i] = audioData[i];
            }
            
            rootMeanSquared /= audioData.length;
            
            //Set amplitude
            amplitude = Math.sqrt(rootMeanSquared);
            
            //Set maximum amplitude
            if (ibiSensorResolutionMicrophone.isNeedMaxAmplitude()) {
                maxAmplitude = maxAmp;        	
            }
            
            //Set minimum amplitude
            if (ibiSensorResolutionMicrophone.isNeedMinAmplitude()) {
            	minAmplitude = minAmp;
            }
            
            //Set mean amplitude
            if (ibiSensorResolutionMicrophone.isNeedMeanAmplitude() || ibiSensorResolutionMicrophone.isNeedVarianceAmplitude()
            		|| ibiSensorResolutionMicrophone.isNeedStandardDeviationAmplitude()
            		) {
            	meanAmplitude = sumAmp/audioData.length;
            }
            
            //Set range
            if (ibiSensorResolutionMicrophone.isNeedRange()) {
            	range = maxAmplitude - minAmplitude;
            }

            if (ibiSensorResolutionMicrophone.isNeedDecibel()) {
            	decibel = 20 * Math.log10(amplitude / MAXIMUM_AMPLITUDE);
            }

        	if (ibiSensorResolutionMicrophone.isNeedVarianceAmplitude() || ibiSensorResolutionMicrophone.isNeedStandardDeviationAmplitude()) {
               
            	double sum = 0.0;
        		
        		for (int i = 0; i < copyAudioData.length; i++) {
                	sum += (copyAudioData[i] - meanAmplitude) * (copyAudioData[i] - meanAmplitude);
                    //Remove DC component to avoid maximum magnitude at frequency zero
                    doubleAudioDataReal[i] = (double) (copyAudioData[i] - meanAmplitude);
                }
                
                varianceAmplitude = (sum / copyAudioData.length);
                standardDeviationAmplitude = Math.sqrt(varianceAmplitude);
        	}

            if (ibiSensorResolutionMicrophone.isNeedFrequency()) {
                //Set frequency by zero crossing method
                /*
                float numberSecondsRecorded = (float) audioData.length / (float) samplingRate;
                float numberCycles = (float) numberZeroCrossing / 2;
                frequency = numberCycles / numberSecondsRecorded;
                */
            	
                //Set frequency by FFT method
                DoubleFFT_1D fft = new DoubleFFT_1D(doubleAudioDataReal.length);
                fft.realForward(doubleAudioDataReal);
                
                double real = 0.0;
                double img = 0.0;
                double[] magnitude = new double[doubleAudioDataReal.length/2];
                
                for (int k = 0; k < (doubleAudioDataReal.length/2); k++) {
             	   real = doubleAudioDataReal[2 * k];
             	   img = doubleAudioDataReal[(2 * k) + 1];
             	   magnitude[k] = Math.sqrt(real*real + img*img);
                }
                
                double maxMagnitude = Double.MIN_VALUE;
                int maxMagnitudeIndex = -1;
                
                for (int i = 0; i < magnitude.length; i++) {
             	   if (magnitude[i] > maxMagnitude) {
             		   maxMagnitude = magnitude[i];
             		   maxMagnitudeIndex = i;
             	   }
                }
                
                frequency = maxMagnitudeIndex * samplingRate / doubleAudioDataReal.length;
            }

        }
        
        /**
         * Calculate the amplitude by root mean squared method
         * 
         * @return amplitude
         */
        private double getAmplitude() {
                // rootMeanSquared
                double rootMeanSquared = 0;
                for (int i = 0; i < audioData.length; i++) {
                        rootMeanSquared += audioData[i] * audioData[i];
                }

                rootMeanSquared /= audioData.length;

                return Math.sqrt(rootMeanSquared);
        }
        
        /**
         * Calculate the frequency by FFT method
         * @return Frequency
         */
        private float getFrequencyByFFT() {
            double[] doubleAudioDataReal = new double[audioData.length];
            double mean = getMeanAmplitude();
            
            //Remove DC component to avoid maximum magnitude at frequency zero
            for (int i = 0; i < audioData.length; i++) {
                doubleAudioDataReal[i] = (double) (audioData[i] - mean);
            }
            
            //FFT method
            DoubleFFT_1D fft = new DoubleFFT_1D(doubleAudioDataReal.length);
            fft.realForward(doubleAudioDataReal);
            
            double real = 0.0;
            double img = 0.0;
            double[] magnitude = new double[doubleAudioDataReal.length/2];
            
            for (int k = 0; k < (doubleAudioDataReal.length/2); k++) {
         	   real = doubleAudioDataReal[2 * k];
         	   img = doubleAudioDataReal[(2 * k) + 1];
         	   magnitude[k] = Math.sqrt(real*real + img*img);
            }
            
            double maxMagnitude = Double.MIN_VALUE;
            int maxMagnitudeIndex = -1;
            
            for (int i = 0; i < magnitude.length; i++) {
         	   if (magnitude[i] > maxMagnitude) {
         		   maxMagnitude = magnitude[i];
         		   maxMagnitudeIndex = i;
         	   }
            }
            
            float frequency = maxMagnitudeIndex * samplingRate / doubleAudioDataReal.length;
            
            return frequency;
        }

        /**
         * Calculate the frequency by zero crossing method
         * 
         * @return frequency
         */
        private float getFrequencyByZeroCrossing() {
                int numberSamples = audioData.length;
                int numberZeroCrossing = 0;
                for (int i = 0; i < numberSamples - 1; i++) {

                        if ((audioData[i] > 0 && audioData[i + 1] <= 0) || (audioData[i] < 0 && audioData[i + 1] >= 0)) {
                                numberZeroCrossing++;
                        }
                }

                float numberSecondsRecorded = (float) numberSamples / (float) samplingRate;
                float numberCycles = (float) numberZeroCrossing / 2;
                float frequency = numberCycles / numberSecondsRecorded;

                return frequency;
        }

        /**
         * Get decibel
         * 
         * @param amplitude
         * @return decibel
         */
        private double getDecibel(double amplitude) {
                return 20 * Math.log10(amplitude / MAXIMUM_AMPLITUDE);
        }

        /**
         * Get maximum amplitude
         * @return Maximum amplitude
         */
        private double getMaxAmplitude() {
            double maxAmplitude = Double.MIN_VALUE;
            
            for (int i = 0; i < audioData.length; i++) {
                    if (maxAmplitude < audioData[i]) {
                    	maxAmplitude = audioData[i];
                    }
            }
            
        	if (DEBUG) System.out.println("Get maximum amplitude = " + maxAmplitude);
            
            return maxAmplitude;
        }
        
        /**
         * Get minimum amplitude
         * @return Minimum amplitude
         */
        private double getMinAmplitude() {
            double minAmplitude = Double.MAX_VALUE;
            
            for (int i = 0; i < audioData.length; i++) {
                    if (minAmplitude > audioData[i]) {
                    	minAmplitude = audioData[i];
                    }
            }

        	if (DEBUG) System.out.println("Get minimum amplitude = " + minAmplitude);
        	
            return minAmplitude;
        }
        
        /**
         * Get range
         * @return Range
         */
        public double getRange() {
        	return (getMaxAmplitude() - getMinAmplitude());
        }
        
        /**
         * Get mean of amplitude
         * @return Mean of amplitude
         */
        private double getMeanAmplitude() {
        	
            double sumAmplitude = 0.0;
            
            for (int i = 0; i < audioData.length; i++) {
            	sumAmplitude += audioData[i];
            }

            return (sumAmplitude / audioData.length);
        }
        
        /**
         * Get variance of amplitude
         * @return Variance of amplitude
         */
        public double getVarianceAmplitude() {

        	double mean = getMeanAmplitude();
        	double sum = 0.0;
        	
        	//Formula of variance (sigma^2)
        	//Variance (sigma^2) = sum((value - mean)^2)/nbValues
            for (int i = 0; i < audioData.length; i++) {
            	sum += (audioData[i] - mean) * (audioData[i] - mean);
            }
        	
        	return (sum / audioData.length);
        }
          
        /**
         * Get standard deviation
         * @return Standard deviation
         */
        public double getStandardDeviationAmplitude() {
        	//Standard deviation = square root of variance
        	return Math.sqrt(getVarianceAmplitude());
        }
        
        /**
         * Stop audio recording
         */
        private void stopRecordingAudio() {
                if (DEBUG) System.out.println("Stop recording audio");

                if (isStarted) {
                        if (threadExecutor != null)
                                threadExecutor.shutdownNow();

                        if (audioRecord != null) {
                                audioRecord.stop();
                                audioRecord.release();
                                audioRecord = null;
                        }
                        isStarted = false;
                }
        }

        //
        //
        //
        //

        private class ReadAudioThread implements Runnable {
                @Override
                public void run() {

                        int numberOfShortsRead = audioRecord.read(audioData, 0, requiredReadBufferSize);

                        if (numberOfShortsRead == AudioRecord.ERROR_INVALID_OPERATION) {
                                if (DEBUG) System.err.println("Object wasn't properly initialized");
                        } else if (numberOfShortsRead == AudioRecord.ERROR_BAD_VALUE) {
                                if (DEBUG) System.err.println("The parameters don't resolve to valid data and indexes");
                        } else {
                                processingAudio();
                        }
                }
        }

}