package org.ibicoop.android.statespace;

import org.ejml.simple.SimpleMatrix;

public class StateSpace {

	//Discrete-time state-space model:
	//x(t+Ts) = A x(t) + B u(t) + K e(t)
	//y(t) = C x(t) + D u(t) + e(t)
	//In our case, we suppose that K = 0 and e = 0
	//newX = A*oldX + B*input
	//output = C*oldX + D*input
	private SimpleMatrix input, output;
	private SimpleMatrix initialX, oldX, newX;
	private SimpleMatrix A, B, C, D;
	
	public StateSpace(double[][] matrixA, double[][] matrixB, 
			double[][] matrixC, double[][] matrixD, 
			double[][] initialState) {
		this(
				new SimpleMatrix(matrixA), new SimpleMatrix(matrixB),
				new SimpleMatrix(matrixC), new SimpleMatrix(matrixD),
				new SimpleMatrix(initialState)
			);
	}
	
	public StateSpace(SimpleMatrix matrixA, SimpleMatrix matrixB, 
			SimpleMatrix matrixC, SimpleMatrix matrixD, 
			SimpleMatrix initialState) {
		A = matrixA;
		B = matrixB;
		C = matrixC;
		D = matrixD;
		initialX = initialState;
		oldX = initialX;
	}
	
	public void updateState(double[][] newInput) {
		updateState(new SimpleMatrix(newInput));
	}
	
	public void updateState(SimpleMatrix newInput) {
		input = newInput;
		//Calculate the new state
		//newX = A*oldX + B*input
		newX = A.mult(oldX).plus(B.mult(input));
		//output = C*oldX + D*input
		output = C.mult(oldX).plus(D.mult(input));
		//Prediction
		//output = C.mult(newX).plus(D.mult(input));
		//Set the oldX
		oldX =  newX;
	}

	public void resetState() {
		oldX = initialX;
		newX = null; //Should check
	}
	
	public SimpleMatrix getOutput() {
		return output;
	}
}
