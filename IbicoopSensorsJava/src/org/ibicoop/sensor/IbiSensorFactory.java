package org.ibicoop.sensor;

import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

/**
 * IbiSensorManager allows user to get the specified sensor and start it directly
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorFactory {

	protected static IbiSensorFactory singleton;

	/**
	 * Get the singleton of IbiSensorManager
	 * 
	 * @return singleton of IbiSensorManager depending on the platform
	 */
	public static IbiSensorFactory getInstance() {

		try {

			if (singleton == null) {

				singleton = (IbiSensorFactory) Class.forName("org.ibicoop.sensor.impl.IbiSensorFactoryImpl").newInstance();

				return singleton;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}

		return singleton;
	}

	/**
	 * Get the specified sensor and start it directly
	 * 
	 * @param context
	 *            Context of the usage
	 * @param type
	 *            Sensor type
	 * @param resolution
	 *            Sensor resolution
	 * @param listener
	 *            Sensor listener
	 * @return
	 */
	public abstract IbiSensor getSensor(Object context, IbiSensorType type, IbiSensorResolution resolution, IbiSensorListener listener) throws IbiSensorException;
}
