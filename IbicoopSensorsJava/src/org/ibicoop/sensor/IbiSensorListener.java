package org.ibicoop.sensor;

/**
 * The listener of sensor which allows to receive connected and disconnected status from the sensor
 * 
 * @author khoo
 * 
 */
public interface IbiSensorListener {

	/**
	 * The sensor is closed for all listenner
	 * 
	 * @param ibiSensor
	 *            Mobile sensor
	 */
	public void stopped(IbiSensor ibiSensor);

	/**
	 * The sensor is started
	 * 
	 * @param ibiSensor
	 *            Mobile sensor
	 */
	public void started(IbiSensor ibiSensor);

	/**
	 * The sensor is paused
	 * 
	 * @param ibiSensor
	 *            Mobile sensor
	 */
	public void paused(IbiSensor ibiSensor);

	/**
	 * The sensor is resumed
	 * 
	 * @param ibiSensor
	 *            Mobile sensor
	 */
	public void resumed(IbiSensor ibiSensor);
}
