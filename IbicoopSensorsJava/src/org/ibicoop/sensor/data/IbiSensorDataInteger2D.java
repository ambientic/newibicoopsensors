package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Two dimension integer type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataInteger2D extends IbiSensorDataInteger1D {

	protected int y;

	/**
	 * Constructor of two dimension integer type sensor data
	 * 
	 * @param x
	 *            sensor data x integer value
	 * @param y
	 *            sensor data y integer value
	 * @param timestamp
	 *            sensor data timestamp in millisecond
	 * @param unit
	 *            sensor data unit
	 */
	public IbiSensorDataInteger2D(int x, int y, long timestamp, IbiSensorUnit unit) {
		super(x, timestamp, unit);
		this.y = y;
	}

	@Override
	public Object getValue() {
		int[] intTab = { x, y };
		return ((int[]) intTab);
	}

	/**
	 * Get the y double value
	 * 
	 * @return y double value
	 */
	public int getY() {
		return y;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXY = ((int[]) otherData.getValue());

		return ((x > otherXY[0]) && (y > otherXY[1]));
	}

	/**
	 * Compares if current (x, y) double value is greater than other (x, y) double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @param otherY
	 *            Other y double value
	 * @return true if current (x, y) double value is greater than other (x, y) double value, false otherwise
	 */
	public boolean isGreaterThan(int otherX, int otherY) {
		return ((x > otherX) && (y > otherY));
	}

	/**
	 * Compares if current y double value is greater than other y double value
	 * 
	 * @param otherY
	 *            Other y double value
	 * @return true if current y double value is greater than other y double value, false otherwise
	 */
	public boolean yIsGreaterThan(int otherY) {
		return (y > otherY);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataInteger2D) || !(sup instanceof IbiSensorDataInteger2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherInfXY = ((int[]) inf.getValue());
		int[] otherSupXY = ((int[]) sup.getValue());

		return ((x >= otherInfXY[0]) && (x <= otherSupXY[0]) && (y >= otherInfXY[1]) && (y <= otherSupXY[1]));
	}

	/**
	 * Compares if the current (x, y) double value is comprised between inferior and superior bound of others (x, y) double values
	 * 
	 * @param xInf
	 *            Inferior bound of other x double value
	 * @param xSup
	 *            Superior bound of other x double value
	 * @param yInf
	 *            Inferior bound of other y double value
	 * @param ySup
	 *            Superior bound of other y double value
	 * @return true if the current (x, y) double value is comprised between inferior and superior bound of others (x, y) double values, false otherwise
	 */
	public boolean isInRange(int xInf, int yInf, int xSup, int ySup) {
		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup));
	}

	/**
	 * Compares if the current y double value is comprised between inferior and superior bound of others y double values
	 * 
	 * @param yInf
	 *            Inferior bound of other y double value
	 * @param ySup
	 *            Superior bound of other y double value
	 * @return true if the current y double value is comprised between inferior and superior bound of others y double values, false otherwise
	 */
	public boolean yIsInRange(int yInf, int ySup) {
		return ((y >= yInf) && (y <= ySup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXY = ((int[]) otherData.getValue());

		return ((x < otherXY[0]) && (y < otherXY[1]));
	}

	public boolean isLessThan(int otherX, int otherY) {
		return ((x < otherX) && (y < otherY));
	}

	public boolean yIsLessThan(int otherY) {
		return (y < otherY);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		int[] otherXY = ((int[]) otherData.getValue());

		return ((x == otherXY[0]) && (y == otherXY[1]));
	}

	public boolean isEqualTo(int otherX, int otherY) {
		return ((x == otherX) && (y == otherY));
	}

	public boolean yIsEqualTo(int otherY) {
		return (y == otherY);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataInteger2D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
