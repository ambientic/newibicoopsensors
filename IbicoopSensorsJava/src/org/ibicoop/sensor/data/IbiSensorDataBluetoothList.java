package org.ibicoop.sensor.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Bluetooth sensor data array
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataBluetoothList extends IbiSensorData {

	private List<IbiSensorDataBluetooth> datas;
	private HashMap<String, String> maps;

	/**
	 * Constructor of bluetooth sensor data array with empty list
	 * 
	 * @param timestamp
	 */
	public IbiSensorDataBluetoothList(long timestamp) {
		this(new ArrayList<IbiSensorDataBluetooth>(), timestamp);
	}

	/**
	 * Constructor of bluetooth sensor data array
	 * 
	 * @param datas
	 * @param timestamp
	 */
	public IbiSensorDataBluetoothList(List<IbiSensorDataBluetooth> datas, long timestamp) {
		super(timestamp, new IbiSensorUnit());
		this.datas = datas;
		maps = new HashMap<String, String>();

		for (IbiSensorDataBluetooth dataBT : datas) {
			maps.put(dataBT.getName(), dataBT.getAddress());
		}
	}

	/**
	 * Add new bluetooth data to the list
	 * 
	 * @param newData
	 */
	public void addIbiSensorDataBluetooth(IbiSensorDataBluetooth newData) {
		// Add new data to the list
		if (!maps.containsKey(newData.getName())) {
			System.out.println(newData.getName());
			datas.add(newData);
			// Update timestamp
			timestamp = System.currentTimeMillis();
			maps.put(newData.getName(), newData.getAddress());
		}
	}

	public void removeIbiSensorDataBluetooth() {
		datas = new ArrayList<IbiSensorDataBluetooth>();
		maps = new HashMap<String, String>();
		// Update timestamp
		timestamp = System.currentTimeMillis();
	}

	@Override
	public Object getValue() {
		return ((List<IbiSensorDataBluetooth>) datas);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < datas.size(); i++) {
			sb.append(datas.get(i).toString() + "\n");
		}

		return sb.toString();
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBluetoothList)) {
			throw new IbiSensorDataIncompatibleException();
		}

		IbiSensorDataBluetoothList otherBluetoothArray = ((IbiSensorDataBluetoothList) otherData);
		List<IbiSensorDataBluetooth> otherBluetoothData = ((List<IbiSensorDataBluetooth>) otherBluetoothArray.getValue());

		if (datas.size() != otherBluetoothData.size())
			return false;

		for (int i = 0; i < datas.size(); i++) {
			if (!datas.get(i).isEqualTo(otherBluetoothData.get(i)))
				return false;
		}

		return true;
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataBluetoothList)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.getTimeStamp());
	}

}
