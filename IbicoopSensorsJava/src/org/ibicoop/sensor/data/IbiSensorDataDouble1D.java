package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * One dimension sensor data of type double
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataDouble1D extends IbiSensorDataNumber {

	protected double x;

	/**
	 * Constructor of one dimension sensor data of type double
	 * 
	 * @param x
	 *            sensor data double value
	 * @param timestamp
	 *            sensor timestamp in millisecond
	 * @param unit
	 *            sensor unit
	 */
	public IbiSensorDataDouble1D(double x, long timestamp, IbiSensorUnit unit) {
		super(timestamp, unit);
		this.x = x;
	}

	@Override
	public Object getValue() {
		double[] doubleTab = { x };
		return ((double[]) doubleTab);
	}

	/**
	 * Get the x double value
	 * 
	 * @return x double value
	 */
	public double getX() {
		return x;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double otherX = ((double[]) otherData.getValue())[0];

		return (x > otherX);
	}

	/**
	 * Compares if current x double value is greater than other x double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @return true if current x double value is greater than other x double value, false otherwise
	 */
	public boolean xIsGreaterThan(double otherX) {
		return (x > otherX);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataDouble1D) || !(sup instanceof IbiSensorDataDouble1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double infX = ((double[]) inf.getValue())[0];
		double supX = ((double[]) sup.getValue())[0];
		return ((x >= infX) && (x <= supX));
	}

	/**
	 * Compares if the current x double value is comprised between inferior and superior bound of others x double values
	 * 
	 * @param infX
	 *            Inferior bound of other x double value
	 * @param supX
	 *            Superior bound of other x double value
	 * @return true if the current x double value is comprised between inferior and superior bound of others x double values, false otherwise
	 */
	public boolean xIsInRange(double infX, double supX) {
		return ((x >= infX) && (x <= supX));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double otherX = ((double[]) otherData.getValue())[0];

		return (x < otherX);
	}

	/**
	 * Compares if current x double value is less than other x double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @return true if current x double value is less than other x double value, false otherwise
	 */
	public boolean xIsLessThan(double otherX) {
		return (x < otherX);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		double otherX = ((double[]) otherData.getValue())[0];

		return (x == otherX);
	}

	/**
	 * Compares if current x double value is equal to other x double value
	 * 
	 * @param otherX
	 *            Other x double value
	 * @return true if current x double value is equal to other x double value, false otherwise
	 */
	public boolean xIsEqualTo(double otherX) {
		return (x == otherX);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataDouble1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}

}
