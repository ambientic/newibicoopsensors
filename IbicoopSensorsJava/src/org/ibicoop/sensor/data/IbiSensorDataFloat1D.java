package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * One dimension sensor data of type float
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataFloat1D extends IbiSensorDataNumber {

	protected float x;

	/**
	 * Constructor of one dimension sensor data of type float
	 * 
	 * @param x
	 *            sensor data x float value
	 * @param timestamp
	 *            sensor timestamp in
	 * @param unit
	 */
	public IbiSensorDataFloat1D(float x, long timestamp, IbiSensorUnit unit) {
		super(timestamp, unit);
		this.x = x;
	}

	@Override
	public Object getValue() {
		float[] floatTab = { x };
		return ((float[]) floatTab);
	}

	/**
	 * Get the x float value
	 * 
	 * @return x float value
	 */
	public float getX() {
		return x;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float otherX = ((float[]) otherData.getValue())[0];

		return (x > otherX);
	}

	/**
	 * Compares if current x float value is greater than other x float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @return true if current x float value is greater than other x float value, false otherwise
	 */
	public boolean xIsGreaterThan(float otherX) {
		return (x > otherX);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataFloat1D) || !(sup instanceof IbiSensorDataFloat1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float infX = ((float[]) inf.getValue())[0];
		float supX = ((float[]) sup.getValue())[0];

		return ((x >= infX) && (x <= supX));
	}

	/**
	 * Compares if the current x float value is comprised between inferior and superior bound of others x float values
	 * 
	 * @param infX
	 *            Inferior bound of other x float value
	 * @param supX
	 *            Superior bound of other x float value
	 * @return true if the current x float value is comprised between inferior and superior bound of others x float values, false otherwise
	 */
	public boolean xIsInRange(float infX, float supX) {
		return ((x >= infX) && (x <= supX));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float otherX = ((float[]) otherData.getValue())[0];

		return (x < otherX);
	}

	/**
	 * Compares if current x float value is less than other x float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @return true if current x float value is less than other x float value, false otherwise
	 */
	public boolean xIsLessThan(float otherX) {
		return (x < otherX);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		float otherX = ((float[]) otherData.getValue())[0];

		return (x == otherX);
	}

	/**
	 * Compares if current x float value is equal to other x float value
	 * 
	 * @param otherX
	 *            Other x float value
	 * @return true if current x float value is equal to other x float value, false otherwise
	 */
	public boolean xIsEqualTo(float otherX) {
		return (x == otherX);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataFloat1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}

}
