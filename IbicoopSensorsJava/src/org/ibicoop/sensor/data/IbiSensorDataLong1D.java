package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * One dimension long type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataLong1D extends IbiSensorDataNumber {

	protected long x;

	/**
	 * Constructor of one dimension long type sensor data
	 * 
	 * @param x
	 *            Sensor data x long value
	 * @param timestamp
	 *            Sensor data timestamp in millisecond
	 * @param unit
	 *            Sensor data unit
	 */
	public IbiSensorDataLong1D(long x, long timestamp, IbiSensorUnit unit) {
		super(timestamp, unit);
		this.x = x;
	}

	@Override
	public Object getValue() {
		long[] longTab = { x };
		return ((long[]) longTab);
	}

	/**
	 * Get the x long value
	 * 
	 * @return x long value
	 */
	public long getX() {
		return x;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long otherX = ((long[]) otherData.getValue())[0];

		return (x > otherX);
	}

	/**
	 * Compares if current x long value is greater than other x long value
	 * 
	 * @param otherX
	 *            Other x long value
	 * @return true if current x long value is greater than other x long value, false otherwise
	 */
	public boolean xIsGreaterThan(long otherX) {
		return (x > otherX);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataLong1D) || !(sup instanceof IbiSensorDataLong1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long infX = ((long[]) inf.getValue())[0];
		long supX = ((long[]) sup.getValue())[0];

		return ((x >= infX) && (x <= supX));
	}

	/**
	 * Compares if the current x long value is comprised between inferior and superior bound of others x long values
	 * 
	 * @param infX
	 *            Inferior bound of other x long value
	 * @param supX
	 *            Superior bound of other x long value
	 * @return true if the current x long value is comprised between inferior and superior bound of others x long values, false otherwise
	 */
	public boolean xIsInRange(long infX, long supX) {
		return ((x >= infX) && (x <= supX));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long otherX = ((long[]) otherData.getValue())[0];

		return (x < otherX);
	}

	/**
	 * Compares if current x long value is less than other x long value
	 * 
	 * @param otherX
	 *            Other x long value
	 * @return true if current x long value is less than other x long value, false otherwise
	 */
	public boolean xIsLessThan(long otherX) {
		return (x < otherX);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long otherX = ((long[]) otherData.getValue())[0];

		return (x == otherX);
	}

	/**
	 * Compares if current x long value is equal to other x long value
	 * 
	 * @param otherX
	 *            Other x long value
	 * @return true if current x long value is equal to other x long value, false otherwise
	 */
	public boolean xIsEqualTo(long otherX) {
		return (x == otherX);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong1D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.getTimeStamp());
	}

}
