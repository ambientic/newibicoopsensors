package org.ibicoop.sensor.data;

import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;
import org.ibicoop.sensor.unit.IbiSensorUnit;

/**
 * Three dimensions long type sensor data
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataLong3D extends IbiSensorDataLong2D {

	protected long z;

	/**
	 * Constructor of three dimensions long type sensor data
	 * 
	 * @param x
	 *            Sensor data x long value
	 * @param y
	 *            Sensor data y long value
	 * @param z
	 *            Sensor data z long value
	 * @param timestamp
	 *            Sensor data timestamp in millisecond
	 * @param unit
	 *            Sensor data unit
	 */
	public IbiSensorDataLong3D(long x, long y, long z, long timestamp, IbiSensorUnit unit) {
		super(x, y, timestamp, unit);
		this.z = z;
	}

	@Override
	public Object getValue() {
		long[] longTab = { x, y, z };
		return ((long[]) longTab);
	}

	public long getZ() {
		return z;
	}

	@Override
	public String toString() {
		return "x = " + x + " " + sensorUnit.toString() + " , y = " + y + " " + sensorUnit.toString() + ", z = " + z + " " + sensorUnit.toString() + ", timestamp = " + timestamp;
	}

	@Override
	public boolean isGreaterThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXYZ = ((long[]) otherData.getValue());

		return ((x > otherXYZ[0]) && (y > otherXYZ[1]) && (z > otherXYZ[2]));
	}

	public boolean isGreaterThan(long otherX, long otherY, long otherZ) {
		return ((x > otherX) && (y > otherY) && (z > otherZ));
	}

	public boolean zIsGreaterThan(long otherZ) {
		return (z > otherZ);
	}

	@Override
	public boolean isInRange(IbiSensorData inf, IbiSensorData sup) throws IbiSensorDataIncompatibleException {

		if (!(inf instanceof IbiSensorDataLong3D) || !(sup instanceof IbiSensorDataLong3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherInfXYZ = ((long[]) inf.getValue());
		long[] otherSupXYZ = ((long[]) sup.getValue());

		return ((x >= otherInfXYZ[0]) && (x <= otherSupXYZ[0]) && (y >= otherInfXYZ[1]) && (y <= otherSupXYZ[1]) && (z >= otherInfXYZ[2]) && (z <= otherSupXYZ[2]));
	}

	public boolean isInRange(long xInf, long yInf, long zInf, long xSup, long ySup, long zSup) {

		return ((x >= xInf) && (x <= xSup) && (y >= yInf) && (y <= ySup) && (z >= zInf) && (z <= zSup));
	}

	public boolean zIsInRange(long zInf, long zSup) {

		return ((z >= zInf) && (z <= zSup));
	}

	@Override
	public boolean isLessThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXYZ = ((long[]) otherData.getValue());

		return ((x < otherXYZ[0]) && (y < otherXYZ[1]) && (z < otherXYZ[2]));
	}

	public boolean isLessThan(long otherX, long otherY, long otherZ) {
		return ((x < otherX) && (y < otherY) && (z < otherZ));
	}

	public boolean zIsLessThan(long otherZ) {
		return (z < otherZ);
	}

	@Override
	public boolean isEqualTo(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		long[] otherXYZ = ((long[]) otherData.getValue());

		return ((x == otherXYZ[0]) && (y == otherXYZ[1]) && (z == otherXYZ[2]));
	}

	public boolean isEqualTo(long otherX, long otherY, long otherZ) {
		return ((x == otherX) && (y == otherY) && (z == otherZ));
	}

	public boolean zIsEqualTo(long otherZ) {
		return (z == otherZ);
	}

	@Override
	public boolean isFresherThan(IbiSensorData otherData) throws IbiSensorDataIncompatibleException {

		if (!(otherData instanceof IbiSensorDataLong3D)) {
			throw new IbiSensorDataIncompatibleException();
		}

		return (timestamp > otherData.timestamp);
	}
}
