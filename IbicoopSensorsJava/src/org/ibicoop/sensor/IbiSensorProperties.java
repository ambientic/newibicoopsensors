package org.ibicoop.sensor;

import java.util.HashMap;

/**
 * Sensor properties
 * 
 * @author khoo
 * 
 */
public class IbiSensorProperties {
	private final static String NAME = "name";
	private final static String MODEL = "model";

	private HashMap<String, String> properties = new HashMap<String, String>();

	/**
	 * Sensor properties are given by sensor name and modem
	 * 
	 * @param name
	 *            Sensor name
	 * @param model
	 *            Sensor model
	 */
	public IbiSensorProperties() {
	}

	/**
	 * Sensor properties are given by sensor name and modem
	 * 
	 * @param name
	 *            Sensor name
	 * @param model
	 *            Sensor model
	 */
	public IbiSensorProperties(String name, String model) {
		properties.put(NAME, name);
		properties.put(MODEL, model);
	}

	/**
	 * Get the sensor name
	 * 
	 * @return sensor name
	 */
	public String getName() {
		return properties.get(NAME);
	}

	/**
	 * Get the sensor model
	 * 
	 * @return sensor model
	 */
	public String getModel() {
		return properties.get(MODEL);
	}

	/**
	 * Get the sensor model
	 * 
	 * @return sensor model
	 */
	public String getProperty(String key) {
		return properties.get(key);
	}

	/**
	 * Get the sensor model
	 * 
	 * @return sensor model
	 */
	public void setPropertie(String key, String value) {
		properties.put(key, value);
	}

}
