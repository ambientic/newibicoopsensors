package org.ibicoop.sensor;

import java.util.Vector;

import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;
import org.ibicoop.sensor.resolution.IbiSensorResolution;

/**
 * Mobile sensor where each type of sensor is a singleton
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensor {

	Vector<IbiSensorListener> listeners = new Vector<IbiSensorListener>();
	IbiSensorProperties properties = new IbiSensorProperties();

	protected IbiSensor() {
	}

	public void start(IbiSensorListener listener) {
		if (!listeners.contains(listener))
			listeners.add(listener);

		// notify the listener in a separate thread. Only the calling listener is notified this time
		new CallbackThread.CallbackSensorListener(listener).started(this);
	}

	/**
	 * Removes the specified listener from the callbacks list for this sensor.
	 * 
	 * @param listener
	 */
	public void unregister(IbiSensorListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Close the sensor for the app. To restart the sensor, user should recreate the IbiSensor instance. If already closed, do nothing. The close automatically removes the SensorListener
	 */
	public void stop() throws IbiSensorException {
		for (IbiSensorListener listener : listeners) {
			new CallbackThread.CallbackSensorListener(listener).stopped(this);
		}
	};

	/**
	 * Pause the sensing. If already paused, do nothing. If sensor closed, raise exception.
	 */
	public void pause() throws IbiSensorException {
		for (IbiSensorListener listener : listeners) {
			new CallbackThread.CallbackSensorListener(listener).paused(this);
		}
	}

	/**
	 * Resume the sensing. If already active, do nothing. If sensor closed, raise exception.
	 */
	public void resume() throws IbiSensorException {
		for (IbiSensorListener listener : listeners) {
			new CallbackThread.CallbackSensorListener(listener).resumed(this);
		}
	}

	/**
	 * Get sensor properties
	 * 
	 * @return
	 */
	public IbiSensorProperties getProperties() throws IbiSensorException {
		return properties;
	}

	/**
	 * Get sensor properties
	 * 
	 * @return
	 */
	protected void setProperties(IbiSensorProperties properties) throws IbiSensorException {
		if (properties == null) {
			throw new IbiSensorException();
		}
		this.properties = properties;
	}

	/**
	 * Get sensor data
	 * 
	 * @return Sensor data
	 */
	abstract public IbiSensorData getData() throws IbiSensorException;

	/**
	 * User can change the resolution of the sensor which invokes the reconfiguration and restarting of the sensor
	 * 
	 * @param resolution
	 *            type of IbiSensoResolution which can be time, distance, sampling rate or location
	 */
	abstract public void changeResolution(IbiSensorResolution resolution) throws IbiSensorException;
}
