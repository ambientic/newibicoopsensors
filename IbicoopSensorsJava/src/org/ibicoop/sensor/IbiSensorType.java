package org.ibicoop.sensor;

/**
 * Sensor type
 * 
 * @author khoo
 * 
 */
public enum IbiSensorType {
	ACCELEROMETER("Accelerometer"), LOCATION("Location"), BLUETOOTH("Bluetooth"), MICROPHONE("Microphone"), METRO("Metro"), BATTERY("Battery"), RADIO("Radio");

	private final String type;

	/**
	 * Sensor type constructor
	 * 
	 * @param type
	 *            sensor type
	 */
	private IbiSensorType(String type) {
		this.type = type;
	}

	/**
	 * Get the string sensor type
	 */
	@Override
	public String toString() {
		return type;
	}
}
