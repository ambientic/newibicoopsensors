package org.ibicoop.sensor.resolution;

/**
 * Sensor location resolution for location sensor includes the location accuracy, location update time and distance
 * 
 * @author khoo
 * 
 */
public class IbiSensorResolutionLocation implements IbiSensorResolution {

	private IbiSensorResolutionLocationAccuracyType locationAccuracy;
	private long timeMillis;
	private float distance;

	/**
	 * Constructor of sensor location resolution
	 * 
	 * @param locationAccuracy
	 *            Location accuracy of type IbiSensorResolutionLocationAccuracyType
	 * @param timeMillis
	 *            Location update time in millisecond
	 * @param distance
	 *            Location update distance in meter
	 */
	public IbiSensorResolutionLocation(IbiSensorResolutionLocationAccuracyType locationAccuracy, long timeMillis, float distance) {
		this.locationAccuracy = locationAccuracy;
		this.timeMillis = timeMillis;
		this.distance = distance;
	}

	@Override
	public Object getValue() {
		String values[] = { locationAccuracy.toString(), String.valueOf(timeMillis), String.valueOf(distance) };
		return ((String[]) values);
	}

	/**
	 * Get the location accuracy
	 * 
	 * @return Location accuracy
	 */
	public IbiSensorResolutionLocationAccuracyType getAccuracy() {
		return locationAccuracy;
	}

	/**
	 * Get location update time in millisecond
	 * 
	 * @return
	 */
	public long getTime() {
		return timeMillis;
	}

	/**
	 * Get location update distance in meter
	 * 
	 * @return Location update distance in meter
	 */
	public float getDistance() {
		return distance;
	}

	@Override
	public String toString() {
		return "Accuracy = " + locationAccuracy.toString() + ", " + "time = " + String.valueOf(timeMillis) + ", " + "distance = " + String.valueOf(distance);
	}

}
