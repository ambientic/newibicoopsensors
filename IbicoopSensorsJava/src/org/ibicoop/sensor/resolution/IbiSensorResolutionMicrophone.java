package org.ibicoop.sensor.resolution;

/**
 * Sensor resolution for microphone includes the sampling rate in Hz and the recording period in millisecond
 * 
 * @author khoo
 * 
 */
public class IbiSensorResolutionMicrophone implements IbiSensorResolution {

	private int samplingRateHz;
	private long recordingPeriodMillis;
	
	private boolean needMaxAmplitude = false;
	private boolean needMinAmplitude = false;
	private boolean needMeanAmplitude = false;
	private boolean needVarianceAmplitude = false;
	private boolean needStandardDeviationAmplitude = false;
	private boolean needRange = false;
	private boolean needDecibel = false;
	private boolean needFrequency = false;

	/**
	 * Constructor of microphone sensor resolution
	 * Provide only amplitude by default
	 * 
	 * @param samplingRateHz
	 *            Sampling rate in Hz
	 * @param recordingPeriodMillis
	 *            Recording period in millisecond
	 */
	public IbiSensorResolutionMicrophone(int samplingRateHz, long recordingPeriodMillis) {
		this.samplingRateHz = samplingRateHz;
		this.recordingPeriodMillis = recordingPeriodMillis;
	}
	
	/**
	 * Constructor of microphone sensor resolution
	 * Provide only amplitude by default
	 * @param samplingRateHz  Sampling rate in Hz
	 * @param recordingPeriodMillis Recording period in millisecond
	 * @param needMaxAmplitude
	 * @param needMinAmpltiude
	 * @param needMeanAmplitude
	 * @param needVarianceAmplitude
	 * @param needStandardDeviationAmplitude
	 * @param needRange
	 * @param needDecibel
	 * @param needFrequency
	 */
	public IbiSensorResolutionMicrophone(int samplingRateHz, long recordingPeriodMillis,
			boolean needMaxAmplitude, boolean needMinAmpltiude,
			boolean needMeanAmplitude, boolean needVarianceAmplitude,
			boolean needStandardDeviationAmplitude, boolean needRange,
			boolean needDecibel, boolean needFrequency
			) 
	{
		this(samplingRateHz, recordingPeriodMillis);
		this.needMaxAmplitude = needMaxAmplitude;
		this.needMinAmplitude = needMinAmpltiude;
		this.needMeanAmplitude = needMeanAmplitude;
		this.needVarianceAmplitude = needVarianceAmplitude;
		this.needStandardDeviationAmplitude = needStandardDeviationAmplitude;
		this.needRange = needRange;
		this.needDecibel = needDecibel;
		this.needFrequency = needFrequency;
	}
	

	/**
	 * Return values of sampling rate and recoring period in String array
	 */
	@Override
	public Object getValue() {
		String values[] = { String.valueOf(samplingRateHz), String.valueOf(recordingPeriodMillis) };
		return ((String[]) values);
	}

	/**
	 * Get sampling rate in Hz
	 * 
	 * @return Sampling rate inHz
	 */
	public int getSamplingRate() {
		return samplingRateHz;
	}

	/**
	 * Get recording period
	 * 
	 * @return Recording period in millisecond
	 */
	public long getRecordingPeriod() {
		return recordingPeriodMillis;
	}

	public boolean isNeedMaxAmplitude() {
		return needMaxAmplitude;
	}

	public boolean isNeedMinAmplitude() {
		return needMinAmplitude;
	}

	public boolean isNeedMeanAmplitude() {
		return needMeanAmplitude;
	}

	public boolean isNeedVarianceAmplitude() {
		return needVarianceAmplitude;
	}

	public boolean isNeedStandardDeviationAmplitude() {
		return needStandardDeviationAmplitude;
	}

	public boolean isNeedRange() {
		return needRange;
	}

	public boolean isNeedDecibel() {
		return needDecibel;
	}

	public boolean isNeedFrequency() {
		return needFrequency;
	}	
	
	@Override
	public String toString() {
		return "Sampling rate = " + String.valueOf(samplingRateHz) + " Hz, " + "Recording period = " + String.valueOf(recordingPeriodMillis) + " ms";
	}
}
