package org.ibicoop.sensor.unit;

/**
 * Sensor unit type
 * @author khoo
 *
 */
public enum IbiSensorUnitType {
	ACCELERATION_UNIT("m/s^2"),
	COORDINATE_UNIT("degree"),
	SPEED_UNIT("m/s"),
	ALTITUDE_UNIT("m"),
	BEARING_UNIT("degree"),
	AMPLITUDE_UNIT(""),
	DECIBEL_UNIT("dB"),
	FREQUENCY_UNIT("Hz"),
	NO_UNIT("");
	
	private final String unit;
	
	/**
	 * Sensor unit type constructor
	 * @param unit String of the unit
	 */
	private IbiSensorUnitType(final String unit) {
		this.unit = unit;
	}
	
	/**
	 * Sensor unit in String
	 * @return Unit in String
	 */
	@Override
	public String toString() {
		return unit;
	}
}
