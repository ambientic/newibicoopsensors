package org.ibicoop.sensor.unit;

/**
 * Sensor data unit
 * @author khoo
 *
 */
public class IbiSensorUnit {

	protected IbiSensorUnitType unit;
	
	/**
	 * Constructor of sensor data unit with no unit
	 */
	public IbiSensorUnit() {
		this(IbiSensorUnitType.NO_UNIT);
	}
	
	/**
	 * Constructor of sensor data unit with specified unit
	 * @param unit Specified sensor data unit of type IbiSensorUnitType
	 */
	public IbiSensorUnit(IbiSensorUnitType unit) {
		this.unit = unit;
	}
	
	/**
	 * Get the unit type
	 * @return Unit type
	 */
	public IbiSensorUnitType getUnitType() {
		return unit;
	}
	
	/**
	 * Return the type string
	 * @return Unit type in string
	 */
	@Override
	public String toString() {
		return unit.toString();
	}
}
