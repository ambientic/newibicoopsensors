package org.ibicoop.sensor.unit;

/**
 * Location sensor unit
 * @author khoo
 *
 */
public class IbiSensorUnitLocation extends IbiSensorUnit {
	
	private IbiSensorUnit coordinateUnit;
	private IbiSensorUnit altitudeUnit;
	private IbiSensorUnit speedUnit;
	private IbiSensorUnit bearingUnit;
	
	/**
	 * Constructor of default location sensor unit
	 */
	public IbiSensorUnitLocation() {
		this(IbiSensorUnitType.COORDINATE_UNIT, IbiSensorUnitType.ALTITUDE_UNIT, IbiSensorUnitType.SPEED_UNIT, IbiSensorUnitType.BEARING_UNIT);
	}
	
	/**
	 * Constructor of location sensor unit
	 * @param coordinateUnitType Unit for latitude and longitude (coordinate)
	 * @param altitudeUnitType Unit for altitude
	 * @param speedUnitType Unit for speed
	 * @param bearingUnitType Unit for bearing
	 */
	public IbiSensorUnitLocation(IbiSensorUnitType coordinateUnitType, IbiSensorUnitType altitudeUnitType, IbiSensorUnitType speedUnitType, IbiSensorUnitType bearingUnitType) {
		super(IbiSensorUnitType.NO_UNIT);
		coordinateUnit = new IbiSensorUnit(coordinateUnitType);
		altitudeUnit = new IbiSensorUnit(altitudeUnitType);
		speedUnit = new IbiSensorUnit(speedUnitType);
		bearingUnit = new IbiSensorUnit(bearingUnitType);
	}
	
	/**
	 * Get the unit of coordinate
	 * @return Sensor unit of coordinate
	 */
	public IbiSensorUnit getCoordinateUnit() {
		return coordinateUnit;
	}
	
	/**
	 * Get the unit of coordinate in String 
	 * @return Sensor unit of coordinate in String
	 */
	public String getCoordinateUnitString() {
		return coordinateUnit.toString();
	}
	
	/**
	 * Get the unit of altitude
	 * @return Sensor unit of altitude
	 */
	public IbiSensorUnit getAltitudeUnit() {
		return altitudeUnit;
	}
	
	/**
	 * Get the unit of altitude in String
	 * @return Sensor unit of altitude in String
	 */
	public String getAltitudeUnitString() {
		return altitudeUnit.toString();
	}
	
	/**
	 * Get the unit of speed
	 * @return Sensor unit of speed
	 */
	public IbiSensorUnit getSpeedUnit() {
		return speedUnit;
	}
	
	/**
	 * Get the unit of speed in String
	 * @return Sensor unit of speed in String
	 */
	public String getSpeedUnitString() {
		return speedUnit.toString();
	}	
	
	/**
	 * Get the unit of bearing
	 * @return Sensor unit of bearing
	 */	
	public IbiSensorUnit getBearingUnit() {
		return bearingUnit;
	}
	
	/**
	 * Get the unit of bearing in String
	 * @return Sensor unit of bearing in String
	 */
	public String getBearingUnitString() {
		return bearingUnit.toString();
	}	
	
}
