package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataNumber;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

/**
 * Over range trigger with not adaptative timer which invokes the trigger when the sensor data is under the threshold
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeUnderRangeTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData underRangeThreshold;

	/**
	 * Constructor of under range trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param threshold
	 *            Threshold in sensor data format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeUnderRangeTrigger(IbiSensor sensor, IbiSensorData threshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		underRangeThreshold = threshold;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {

		boolean isUnderRange = false;

		try {
			IbiSensorDataNumber sensorDataNumber = ((IbiSensorDataNumber) sensorData);
			isUnderRange = sensorDataNumber.isLessThan(underRangeThreshold);
		} catch (IbiSensorDataIncompatibleException exception) {
			System.err.println(exception.getMessage());
		}

		return isUnderRange;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
