package org.ibicoop.sensor.trigger;


import java.util.ArrayList;
import java.util.List;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorDataLocation;

/**
 * Trigger which uses device in region monitoring system
 * @author khoo
 *
 */
public abstract class IbiSensorDataInRegionSystemTrigger {
	
	protected static IbiSensorDataInRegionSystemTrigger singleton;
	protected IbiSensor ibiSensor;
	protected IbiSensorDataInRegionSystemTriggerListener triggerListener;
	protected List<IbiSensorDataLocation> thresholds;
	protected long alertActiveTimeMs;

	/**
	 * Get instance of ibisensor data in region system triggerr
	 * @return IbiSensorDataInRegionSystemTrigger instance
	 */
	public static IbiSensorDataInRegionSystemTrigger getInstance() {
		try {
			if (singleton == null) {
				singleton = (IbiSensorDataInRegionSystemTrigger) Class.forName("org.ibicoop.android.sensor.impl.IbiAndroidSensorDataInRegionSystemTrigger").newInstance();
			}			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return singleton;
	}
	
	/**
	 * Initialize IbiSensorDataInRegionSystemTrigger
	 * @param ibiSensor IbiSensor location 
	 * @param alertActiveTime In region trigger active time
	 * @param listener IbiSensorDataInRegionSystemTriggerListener
	 */
	public abstract void init(IbiSensor ibiSensor, long alertActiveTime, IbiSensorDataInRegionSystemTriggerListener listener);
	
	/**
	 * Add location threshold and call the system
	 * @param threshold
	 */
	public void addThreshold(IbiSensorDataLocation threshold) {
		if (thresholds == null) thresholds = new ArrayList<IbiSensorDataLocation>();
		thresholds.add(threshold);
		callSystem(threshold, true);
	}
	
	/**
	 * Remove location threshold by calling the system
	 * @param threshold
	 */
	public void removeThreshold(IbiSensorDataLocation threshold) {
		//Return if the threshold has not been added
		if ((thresholds == null) || !(thresholds.contains(threshold))) return;
		thresholds.remove(threshold);
		callSystem(threshold, false);
	}
	
	/**
	 * Set new location thresholds
	 * @param newThresholds
	 */
	public void setThreshold(List<IbiSensorDataLocation> newThresholds) {
		thresholds = new ArrayList<IbiSensorDataLocation>();
		thresholds.addAll(newThresholds);
		
		for (int i = 0; i < thresholds.size(); i++) {
			callSystem(thresholds.get(i), true);
		}
	}
	
	/**
	 * Remove all thresholds
	 */
	public void removeAllThresholds() {
		if (thresholds == null) return;
		
		for (IbiSensorDataLocation dataLocation: thresholds) {
			callSystem(dataLocation, false);
		}
		
		thresholds.clear();
	}
	
	/**
	 * Get thresholds
	 * @return thresholds
	 */
	public List<IbiSensorDataLocation> getThresholds() {
		return thresholds;
	}
	
	/**
	 * Pass threshold to system
	 * @param threshold
	 * @param add True if we want to add the threshold, false if we want to remove it
	 */
	public abstract void callSystem(IbiSensorDataLocation threshold, boolean add);

	/**
	 * Receive the callback from the device location monitoring system
	 * @param inOrOut true for in, false for out
	 * @param callbackData
	 */
	public abstract void receiveSystemCallback(boolean inOrOut, IbiSensorDataLocation callbackData);
}
