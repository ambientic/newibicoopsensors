package org.ibicoop.sensor.trigger;

import java.util.concurrent.TimeUnit;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.exceptions.IbiSensorException;

/**
 * Not adaptative timer trigger where the sensor data checking period is fixed. It triggers when sensor data meet the threshold condition
 * 
 * @author khoo
 * 
 */
public abstract class IbiSensorDataTimerNotAdaptativeTrigger extends IbiSensorDataTimerTrigger {

	protected NonAdaptativeTriggerThread nonAdaptativeTriggerThread;

	/**
	 * Constructor of not adaptative timer trigger
	 * 
	 * @param sensor
	 *            Sensor
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeTrigger(IbiSensor sensor, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		nonAdaptativeTriggerThread = new NonAdaptativeTriggerThread();
		threadExecutor.scheduleAtFixedRate(nonAdaptativeTriggerThread, 0, checkTriggerPeriod, TimeUnit.MILLISECONDS);
	}
	
	public void changePeriod(long newPeriod) {
		checkTriggerPeriod = newPeriod;
		threadExecutor.scheduleAtFixedRate(nonAdaptativeTriggerThread, 0, checkTriggerPeriod, TimeUnit.MILLISECONDS);
	}
	
	/**
	 * Thread which compares sensor data and threshold with the fixed period. It invokes the trigger listener when the sensor data meets the condition of threshold
	 * 
	 * @author khoo
	 * 
	 */
	protected class NonAdaptativeTriggerThread implements Runnable {
		@Override
		public void run() {
			IbiSensorData data = null;
			try {
				data = ibiSensor.getData();
				if (checkTrigger(data)) {
					// System.out.println(".....IbiSensorDataTimerNonAdaptativeTrigger..... ");
					triggerListener.ibiSensorDataTrigger(ibiSensor, IbiSensorDataTimerNotAdaptativeTrigger.this, data);
				}
			} catch (IbiSensorException e) {
			}
		}
	}

}
