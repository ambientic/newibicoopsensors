package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;

/**
 * New data trigger with not adaptative timer which triggers when the current data is fresher than the previous one
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeNewDataTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData threshold;
	protected long thresholdTimeStamp;

	/**
	 * Constructor of new data trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param threshold
	 *            Initial sensor data
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeNewDataTrigger(IbiSensor sensor, IbiSensorData threshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		this.threshold = threshold;
		thresholdTimeStamp = threshold.getTimeStamp();
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {
		boolean isFresher = false;

		try {
			// isFresher = sensorData.isFresherThan(threshold);
			isFresher = (sensorData.getTimeStamp() > thresholdTimeStamp);

			if (isFresher) {
				// refreshThreshold(sensorData);
				thresholdTimeStamp = sensorData.getTimeStamp();
			}
		}
		// catch(IbiSensorDataIncompatibleException exception) {
		catch (Exception exception) {
			System.err.println(exception.getMessage());
		}

		return isFresher;
	}

	/**
	 * Refresh the current threshold by the latest sensor data data
	 * 
	 * @param sensorData
	 *            Latest sensor data
	 */
	protected void refreshThreshold(IbiSensorData sensorData) {
		threshold = sensorData;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}
}