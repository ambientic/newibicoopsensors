package org.ibicoop.sensor.trigger;

import org.ibicoop.sensor.IbiSensor;
import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.data.IbiSensorDataNumber;
import org.ibicoop.sensor.exceptions.IbiSensorDataIncompatibleException;

/**
 * Over range trigger with not adaptative timer which invokes the trigger when the sensor data is over the threshold
 * 
 * @author khoo
 * 
 */
public class IbiSensorDataTimerNotAdaptativeOverRangeTrigger extends IbiSensorDataTimerNotAdaptativeTrigger {

	protected IbiSensorData overRangeThreshold;

	/**
	 * Constructor of over range trigger with not adaptative timer
	 * 
	 * @param sensor
	 *            Sensor
	 * @param threshold
	 *            Threshold in sensor data format
	 * @param listener
	 *            Trigger listener
	 * @param period
	 *            Fixed checking period in millisecond
	 * @param timeout
	 * 				Timeout in millisecond, -1 if we don't want timeout
	 */
	public IbiSensorDataTimerNotAdaptativeOverRangeTrigger(IbiSensor sensor, IbiSensorData threshold, IbiSensorDataTriggerListener listener, long period, long timeout) {
		super(sensor, listener, period, timeout);
		overRangeThreshold = threshold;
	}

	@Override
	public boolean checkTrigger(IbiSensorData sensorData) {

		boolean isOverRange = false;

		try {
			IbiSensorDataNumber sensorDataNumber = ((IbiSensorDataNumber) sensorData);
			isOverRange = sensorDataNumber.isGreaterThan(overRangeThreshold);
		} catch (IbiSensorDataIncompatibleException exception) {
			System.err.println(exception.getMessage());
		}

		return isOverRange;
	}

	@Override
	public void terminate() {
		// TODO Auto-generated method stub

	}

}
