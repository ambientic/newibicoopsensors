package org.ibicoop.sensor;

import org.ibicoop.sensor.data.IbiSensorData;
import org.ibicoop.sensor.trigger.IbiSensorDataTrigger;
import org.ibicoop.sensor.trigger.IbiSensorDataTriggerListener;

public class CallbackThread {

	static public class CallbackSensorListener implements Runnable, IbiSensorListener {

		private static final int STOPPED = 1;
		private static final int STARTED = 2;
		private static final int PAUSED = 3;
		private static final int RESUMED = 4;

		private int currentMethod;
		private IbiSensorListener listener;
		private IbiSensor ibiSensor;

		public CallbackSensorListener(IbiSensorListener listener) {
			this.listener = listener;
		}

		@Override
		public void run() {
			switch (currentMethod) {
			case STOPPED:
				listener.stopped(ibiSensor);
				break;
			case STARTED:
				listener.started(ibiSensor);
				break;
			case PAUSED:
				listener.paused(ibiSensor);
				break;
			case RESUMED:
				listener.resumed(ibiSensor);
				break;
			}
		}

		@Override
		public void stopped(IbiSensor ibiSensor) {
			this.ibiSensor = ibiSensor;
			currentMethod = STOPPED;
			Thread newThr = new Thread(this);
			newThr.start();
		}

		@Override
		public void started(IbiSensor ibiSensor) {
			this.ibiSensor = ibiSensor;
			currentMethod = STARTED;
			Thread newThr = new Thread(this);
			newThr.start();
		}

		@Override
		public void paused(IbiSensor ibiSensor) {
			this.ibiSensor = ibiSensor;
			currentMethod = PAUSED;
			Thread newThr = new Thread(this);
			newThr.start();
		}

		@Override
		public void resumed(IbiSensor ibiSensor) {
			this.ibiSensor = ibiSensor;
			currentMethod = RESUMED;
			Thread newThr = new Thread(this);
			newThr.start();
		}

	}

	static public class CallbackTriggerListener implements Runnable, IbiSensorDataTriggerListener {

		private static final int DATATRIGGER = 1;

		private int currentMethod;
		private IbiSensorDataTriggerListener listener;
		private IbiSensor ibiSensor;
		private IbiSensorDataTrigger dataTrigger;
		private IbiSensorData ibiSensorData;

		public CallbackTriggerListener(IbiSensorDataTriggerListener listener) {
			this.listener = listener;
		}

		@Override
		public void run() {
			switch (currentMethod) {
			case DATATRIGGER:
				listener.ibiSensorDataTrigger(ibiSensor, dataTrigger, ibiSensorData);
				break;
			}
		}

		@Override
		public void ibiSensorDataTrigger(IbiSensor ibiSensor, IbiSensorDataTrigger dataTrigger, IbiSensorData ibiSensorData) {
			this.ibiSensor = ibiSensor;
			this.dataTrigger = dataTrigger;
			this.ibiSensorData = ibiSensorData;
			currentMethod = DATATRIGGER;
			Thread newThr = new Thread(this);
			newThr.start();
		}

	}
}
